# **ColdIgniter Toolkit**
### CodeIgniter toolkit for faster development.
by [Simplifie](http://www.simplifie.net "Simplifie") 
and [RockMaya](http://www.rockmaya.com "RockMaya").

---

### APIs

Please refer to our [wiki](https://bitbucket.org/mamoin/coldigniter-toolkit/wiki/Home "ColdIgniter Toolkit APIs").

---

### Contributors
- Mark N. Amoin [admin@simplfiie.net](mailto:admin@simplfiie.net), [prezire@gmail.com](mailto:prezire@gmail.com), [mark@rockmaya.com](mailto:mark@rockmaya.com)
- Randolph Burgos [randolph@rockmaya.com](mailto:randolph@rockmaya.com)

---

### Support development of ColdIgniter Toolkit
[![Support development](https://www.paypal.com/en_US/i/btn/btn_donateCC_LG.gif "Support Development")](https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=prezire%40gmail%2ecom&lc=PH&item_name=Simplifie&currency_code=PHP&bn=PP%2dDonationsBF%3abtn_donateCC_LG%2egif%3aNonHosted)