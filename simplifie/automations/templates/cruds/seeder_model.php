<?php echo '<?php'; ?>
  if(defined('BASEPATH')) exit('No direct script access allowed');
  class <?php echo strtoupper($entity); ?>SeederModel extends SeederModel
  {
    public function __construct()
    {
      $this->setTableName('<?php echo $entity; ?>');
    }
    public final function run($direction)
    {
      for($i = 0; $i < 15; $i++)
      {
        $r = sha1(rand(0, 9999) . date('h:i:sa'));
        $s = 'test user ' . $r;
        $this->db->insert($this->getTableName(), ['full_name' => $s]);
      }
    }
  }