<div id="<?php echo $entity; ?>" class="index row">
	<div class="small-12 medium-12 large-12 columns">
		<?php
			$ctrl = str_replace('_', '', $entity);
			$cml = camelize($entity);
		?>
	<h4><?php $title = humanize(plural($entity)); echo $title; ?></h4>
	  	<a href="<?php echo '<?php echo site_url(\'' . $ctrl . '/create\'); ?>'; ?>" class="button small">New <?php echo singular($title); ?></a>
		
	      <?php
	        $first = substr($cml, 0, 1);
	        echo '<?php foreach($' . plural($cml) . ' as $' . $first . '){ ?>';
	      ?>
	      		
	      		<div class="row panel">
					<?php 
						foreach($fields as $f)
						{
					?><div class="small-12 medium-12 large-6 columns"><?php echo '<?php echo $' . $first . '->' . $f['name'] . '; ?>'; ?></div>
					<?php } ?><div class="small-12 medium-12 large-12 columns">
						<a href="<?php echo "<?php echo site_url('$ctrl" . "/update/' . $" . "$first" . "->" . 'id); ?>'; ?>" class="button small">Update</a>
						<a href="<?php echo "<?php echo site_url('$ctrl" . "/delete/' . $" . "$first" . "->" . 'id); ?>'; ?>" class="button small alert delete">Delete</a>
					</div>
				</div>
	      <?php echo '<?php } ?>'; ?>
	      
	</div>
</div>