<?php 
  echo '<?php';
  $cml = camelize($entity);
  $className = $cml;
  $first = strtoupper(substr($className, 0, 1));
  $className = $first . substr($className, 1, strlen($className));
  $plr = plural($entity);
  $initial = substr($entity, 0, 1);
?>
	if(!defined('BASEPATH')) exit('Direct script access not allowed.');
	class <?php echo $className; ?>Model extends CI_Model
	{
		public function __construct()
		{
			parent::__construct();
		}
		public final function index()
		{
			return $this->db->get('<?php echo $plr . ' ' . $initial; ?>');
		}
		public final function create($data)
		{
			$this->db->insert
			(
				'<?php echo $plr; ?>', 
				$data
			);
			return $this->read($this->db->insert_id());
		}
		public final function read($id)
		{
      return $this->db->get_where
      [
        '<?php echo $plr; ?>', 
        ['id' => $id]
      ];
		}
		<?php 
			if(count($files) > 0){
				foreach($files as $f){
					$key = $f[0];
					$val = $f[1]; 
					$methName = str_replace(' ', '', humanize($key));
					$vars = camelize($key);
		?>public final function upload<?php echo $methName; ?>($<?php echo $entity; ?>Id)
	    {
	      //TODO: Query and remove prev image file.
	      $<?php echo $vars; ?> = upload('<?php echo $vars; ?>');
	      if(isset($<?php echo $vars; ?>))
	      {
	        $a = array('<?php echo $vars; ?>' => $<?php echo $vars; ?>['file_name']);
	        $this->db->where('id', $id);
	        $this->db->update('<?php echo $plr; ?>', $a);
	      }
	    }<?php }} ?>

		public final function update($id, $data)
		{
			$this->db->where('id', $id);
			$this->db->update
			(
				'<?php echo $plr; ?>', 
				$data
			);
      return $this->db->affected_rows() > 0;
		}
		public final function delete($id)
    {
    	$this->db->where('id', $id);
			return $this->db->delete('<?php echo $plr; ?>');
    }
	}