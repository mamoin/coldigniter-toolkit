<?php 
  echo '<?php';
  //
  $cml = camelize($entity);
  $className = $cml;
  $first = strtoupper(substr($className, 0, 1));
  $className = $first . substr($className, 1, strlen($className));
  $ctrlName = strtolower($cml);
  $mdl = $ctrlName . 'model';
  $plr = plural($entity);
?> 
  //<?php echo $command; ?>

  use simplifie\helpers\ViewHelper;
  
  if(!defined('BASEPATH')) exit('No direct script access allowed');
  class <?php echo $className; ?> extends CI_Controller 
  {
  	public function __construct()
  	{
  		parent::__construct();
      $this->load->model('<?php echo $mdl; ?>');
  	}
    public final function index()
    {
      $o = $this-><?php echo $mdl; ?>->index()->result();
      ViewHelper::show('<?php echo $plr; ?>/index', ['<?php echo plural($cml); ?>' => $o]);
    }
    public final function create()
    {
      $a = [];
      if($this->input->post())
      {
        if($this->form_validation->run('<?php echo $entity; ?>/create'))
        {
          $r = $this-><?php echo $mdl; ?>->create($this->input->post());
          if($r->num_rows())
          {
            $o = $r->row();
            redirect(site_url('<?php echo $ctrlName; ?>/update/' . $o->id));
          }
          else
          {
            $a['status'] = 'failed';
            $a['message'] = 'Error creating <?php echo $entity; ?>.';
            ViewHelper::show('<?php echo $plr; ?>/create', $a);
          }
        }
        else
        {
          $a['status'] = 'failed';
          $a['message'] = validation_errors();
          ViewHelper::show('<?php echo $plr; ?>/create', $a);
        }
      }
      else
      {
        ViewHelper::show('<?php echo $plr; ?>/create', $a);
      }
    }
  	public final function read($id)
  	{
  		ViewHelper::show('<?php echo $plr; ?>/read', ['<?php echo $cml; ?>' => $this-><?php echo $mdl; ?>->read($id)->row()]);
  	}
  	public final function update($id = NULL)
    {
      $o = $this-><?php echo $mdl; ?>->read($id)->row();
      $a = ['<?php echo $cml;?>' => $o];
      if($this->input->post())
      {
        if($this->form_validation->run('<?php echo $entity; ?>/update'))
        {
          $this-><?php echo $mdl; ?>->update($id, $this->input->post());
          $o = $this-><?php echo $mdl; ?>->read($this->input->post('id'))->row();
          $a['<?php echo $cml;?>'] = $o;
          $a['status'] = 'success';
          $a['message'] = 'Successfully updated.';
          ViewHelper::show('<?php echo $plr; ?>/update', $a);
        }
        else
        {
          $a['status'] = 'failed';
          $a['message'] = validation_errors();
          ViewHelper::show('<?php echo $plr; ?>/update', $a);
        }
      }
      else
      {
        ViewHelper::show('<?php echo $plr; ?>/update', $a);
      }
    }
  	public final function delete($id, $format = 'html')
    {
      switch($format)
      {
        case 'html':
          $this-><?php echo $mdl; ?>->delete($id);
          redirect(site_url('<?php echo $ctrlName; ?>'));
        break;
        case 'json':
          ViewHelper::json(['<?php echo $cml; ?>' => $this-><?php echo $mdl; ?>->delete($id)->row()]);
        break;
      }
    }
  }