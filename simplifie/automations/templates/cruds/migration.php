<?php 
  echo '<?php';
  $cml = camelize($entity);
  $className = $cml;
  $first = strtoupper(substr($className, 0, 1));
  $className = $first . substr($className, 1, strlen($className));
  $plr = plural($entity);
  $initial = substr($entity, 0, 1);
?>
  
  if(!defined('BASEPATH')) exit('No direct script access allowed.');
  class Migration_<?php echo plural($className); ?> extends CI_Migration
  {
    public final function up()
    {
      $this->dbforge->add_field([
        'id' => 
        [
          'type' => 'INT',
          'constraint' => 30,
          'unsigned' => TRUE,
          'auto_increment' => TRUE
        ]
        <?php
          $i = 0;
          $j = count($fields) - 1;
          foreach($fields as $f)
          {
            $l = explode( ':', $f );
            $key = $l[0];
            if('id' !== $key)
            {
              $val = $l[1];
              $vals = ['enum', 'ref', 'id'];
              if(!in_array($val, $vals))
              {
                //Don't display comma on last item.
                if($i < $j)
                {
echo ', '; 
                }
        ?>
        '<?php echo $key; ?>' => 
        [
          'type' => '<?php echo strtoupper($val); ?>',
          'constraint' => 255,
        ]
        <?php 
              }
            }
            $i++;
          }
         ?>
]);
      $this->dbforge->add_key('id', TRUE);
      $this->dbforge->create_table('<?php echo $plr; ?>');
    }
    public final function down()
    {
      $this->dbforge->drop_table('<?php echo $plr; ?>');
    }
  }