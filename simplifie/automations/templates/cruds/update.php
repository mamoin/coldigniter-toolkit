<div id="<?php echo $entity; ?>" class="update row">
  <div class="small-12 medium-12 large-12 columns">
    <h4>Update <?php echo humanize($entity); ?></h4>
    <?php 
      $ctrl = str_replace('_', '', $entity);
      $cml = camelize($entity);
    ?>
    <?php echo '<?php'; ?> 
        if(isset($status))
        {
          echo $this->load->view
          (
            'commons/partials/header_messages', 
            array('status' => $status), 
            true
          );
        }
        echo form_open('<?php echo $ctrl; ?>/update'); 
      <?php echo '?>'; ?>
      
      <div class="row">  
    <?php foreach($fields as $f){ ?>
      <div class="small-12 medium-12 large-12 columns">
            <?php echo humanize($f['name']); ?>: <?php echo $f['field']; ?>
        
        </div>
    <?php } ?>
    </div>
      <div class="row">
        <div class="small-12 medium-12 large-12 columns">
          <a href="<?php echo '<?php echo site_url('; ?>'<?php echo $ctrl; ?>/read/' <?php echo ' . $' . $cml . '->id'; ?>); <?php echo '?>'; ?>" class="button small alert">Back</a>
          <button class="button small">Update</button>
        </div>
      </div>
    </form>
  </div>
</div>