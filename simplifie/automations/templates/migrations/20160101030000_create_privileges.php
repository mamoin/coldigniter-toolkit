<?php
  if(!defined('BASEPATH')) exit('Direct script access not allowed.');
  class Migration_create_privileges extends CI_Migration
  {
    public final function up()
    {
      $this->dbforge->add_field([
        'id' => 
        [
          'type' => 'INT',
          'constraint' => 30,
          'unsigned' => TRUE,
          'auto_increment' => TRUE
        ],
        'name' => 
        [
          'type' => 'VARCHAR',
          'constraint' => 50
        ]
      ]);
      $this->dbforge->add_key('id', TRUE);
      $this->dbforge->create_table('privileges');
    }
    public final function down()
    {
      $this->dbforge->drop_table('privileges');
    }
  }