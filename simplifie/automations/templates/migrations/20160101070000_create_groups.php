<?php	defined('BASEPATH') OR exit('No direct script access allowed');
	class Migration_groups extends CI_Migration
	{
		public final function up()
		{
			$this->dbforge->add_field([
	      'id' => 
        [
          'type' => 'INT',
          'constraint' => 30,
          'unsigned' => TRUE,
          'auto_increment' => TRUE
	      ]
					      ,
	      'int' => [
          'type' => 'id',
          'constraint' => '300',
	      ]
	      	      ,
	      'varchar' => [
          'type' => 'name',
          'constraint' => '300',
	      ]
	      	      ,
	      'text' => [
          'type' => 'description',
          'constraint' => '300',
	      ]
	      	    ]);
			$this->dbforge->add_key('id', TRUE);
      $this->dbforge->create_table('groups');
		}
		public final function down()
		{
			$this->dbforge->drop_table('groups');
		}
	}