<?php
  if(!defined('BASEPATH')) exit('Direct script access not allowed.');
  class Migration_create_permission_privileges extends CI_Migration
  {
    public final function up()
    {
      $this->dbforge->add_field([
        'id' => 
        [
          'type' => 'INT',
          'constraint' => 30,
          'unsigned' => TRUE,
          'auto_increment' => TRUE
        ],
        'user_id' => 
        [
          'type' => 'INT',
          'constraint' => 30,
          'unsigned' => TRUE
        ],
        'permission_id' => 
        [
          'type' => 'INT',
          'constraint' => 30,
          'unsigned' => TRUE
        ],
        'privilege_id' => 
        [
          'type' => 'INT',
          'constraint' => 30,
          'unsigned' => TRUE
        ],
      ]);
      $this->dbforge->add_key('id', TRUE);
      $this->dbforge->add_key('user_id', TRUE);
      $this->dbforge->add_key('permission_id', TRUE);
      $this->dbforge->add_key('privilege_id', TRUE);
      $this->dbforge->create_table('permission_privileges');
    }
    public final function down()
    {
      $this->dbforge->drop_table('permission_privileges');
    }
  }