<?php
  if(!defined('BASEPATH')) exit('Direct script access not allowed.');
  class Migration_create_user_roles extends CI_Migration
  {
    public final function up()
    {
      $this->dbforge->add_field([
        'id' => 
        [
          'type' => 'INT',
          'constraint' => 30,
          'unsigned' => TRUE,
          'auto_increment' => TRUE
        ],
        'user_id' => 
        [
          'type' => 'INT',
          'constraint' => 30,
          'unsigned' => TRUE
        ],
        'role_id' => 
        [
          'type' => 'INT',
          'constraint' => 30,
          'unsigned' => TRUE
        ]
      ]);
      $this->dbforge->add_key('id', TRUE);
      $this->dbforge->add_key('user_id', TRUE);
      $this->dbforge->add_key('role_id', TRUE);
      $this->dbforge->create_table('user_roles');
    }
    public final function down()
    {
      $this->dbforge->drop_table('user_roles');
    }
  }