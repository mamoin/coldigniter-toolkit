<?php
  if(!defined('BASEPATH')) exit('Direct script access not allowed.');
  class Migration_create_user extends CI_Migration
  {
    public final function up()
    {
      $this->dbforge->add_field([
        'id' => 
        [
          'type' => 'INT',
          'constraint' => 30,
          'unsigned' => TRUE,
          'auto_increment' => TRUE
        ],
        'full_name' => 
        [
          'type' => 'VARCHAR',
          'constraint' => 50
        ],
        'email' => 
        [
          'type' => 'VARCHAR',
          'constraint' => 50
        ],
        'password' => 
        [
          'type' => 'VARCHAR',
          'constraint' => 130
        ],
        'gender' =>
        [
          'type' => 'ENUM'
        ],
        'enabled' =>
        [
          'type' => 'TINYINT',
          'constraint' => 1
        ]
      ]);
      $this->dbforge->add_key('id', TRUE);
      $this->dbforge->create_table('users');
    }
    public final function down()
    {
      $this->dbforge->drop_table('users');
    }
  }