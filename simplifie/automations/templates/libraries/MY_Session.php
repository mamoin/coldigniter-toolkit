<?php
  if(!defined('BASEPATH')) exit('No direct script access allowed');
  class MY_Session extends CI_Session
  {
    public function __construct($params = [])
    {
      parent::__construct($params);
    }

    public final function all_userdata()
    {
      /*
        @var $a   Just making sure we recover
        the data before doing anything that
        involves closing the session mechanism.
      */
      $a = parent::all_userdata();
      session_write_close();
      return $a;
    }

    public final function userdata($item = NULL)
    {
      $a = parent::userdata($item);
      session_write_close();
      return $a;
    }

    public final function set_userdata($data = [], $value = NULL)
    {
      $a  = parent::set_userdata($data, $value);
      session_write_close();
      return $a;
    }

    public function set_lifetime($lifetime = 0)
    {
      session_write_close();
      //$this->__construct(['cookie_lifetime' => $lifetime]);
      parent::set_lifetime($lifetime);
    }
  }

  /* End of file MY_Session.php */
  /* Location: ./application/libraries/Session/MY_Session.php */