<?php
  if(!defined('BASEPATH')) exit('No direct script access allowed');
  class Seeder
  {
    const DATE_TIME_LENGTH = 15;
    private $seeders;
    public function __construct()
    {
      $this->seeders = array();
    }
    public final function setSeeders($seeders)
    {
      if(!empty($seeders))
        $this->seeders = array_unique($seeders);
    }
    public final function getSeeders()
    {
      $b = empty($this->seeders) ?
            array() :
            $this->seeders;
      return $b;
    }
    private final function trimFilename($filename)
    {
      $i_first = strpos($filename, '_');
      $i_last = strlen($filename) - (strlen('.php') + self::DATE_TIME_LENGTH);
      return substr($filename, $i_first + 1, $i_last);
    }
    private final function truncate_all()
    {
      $dir = APPPATH . 'seeders/';
      $seeders = array_diff(scandir($dir), ['.', '..']);
      foreach($seeders as $s)
      {
        require_once $dir . $s;
        $s_mdl = $this->trimFilename($s);
        $mdl = new $s_mdl;
        $mdl->truncate();
      }
    }
    /**
     * Factory pattern. Triggers the seeder.
     * @param  string $specific_seeder Command pattern. A
     * single seeder model to execute. If no specific
     * seeder supplied, the trigger will use all
     * added seeders. Default is NULL.
     * @param string $direction Either up or down.
     * Default is up.
     * @return NULL
     */
    public final function run(
      $specific_seeder = NULL,
      $direction = 'up',
      &$successful_seeders = NULL,
      $truncate_all = FALSE
    )
    {
      $CI = get_instance();
      //Load base model used by child seeder models.
      $CI->load->model('seedermodel', '', TRUE);
      $dir = APPPATH . 'seeders/';
      $s_mdl = '';
      //Disable strict mode when truncating.
      $CI->db->query('SET FOREIGN_KEY_CHECKS = 0');
      //
      if(TRUE === $truncate_all)
      {
        $this->truncate_all();
      }
      //
      if($specific_seeder)
      {
        require_once $dir . $specific_seeder;
        $s_mdl = $this->trimFilename($specific_seeder);
        //Create model once.
        $mdl = new $s_mdl;
        $mdl->truncate();
        $mdl->run($direction);
      }
      else
      {
        $seeders = $this->getSeeders();
        $a_successful_seeders = array();
        foreach($seeders as $s)
        {
          require_once $dir . $s;
          $s_mdl = $this->trimFilename($s);
          //Create multiple models.
          $mdl = new $s_mdl;
          $mdl->truncate();
          $mdl->run($direction);
          array_push($a_successful_seeders, $s);
        }
        $successful_seeders = $a_successful_seeders;
      }
      $CI->db->query('SET FOREIGN_KEY_CHECKS = 1');
    }
  }