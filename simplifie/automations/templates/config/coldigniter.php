<?php
  $config['defaultAssets'] = 
  [
    'js'  => ['/public/vendors/jQuery.js'],
    'css' => ['/public/vendors/base.css']
  ];
  $config['excludedSeeds'] = 
  [
    'environments' => 
    [
      'development' => []
    ]
  ];
  $config['internalAdministrators'] = 
  [
    'credentials' => ['u1/p1']
  ];
  $config['whitelistEnvironments'] = ['development'];
  $config['payments'] = 
  [
    'payPal' => ['adminEmail' => '']
  ];