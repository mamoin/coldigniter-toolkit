<?php
  if(!defined('BASEPATH')) exit('Direct script access not allowed.');
  class PermissionSeederModel extends SeederModel
  {
    public function __construct()
    {
      $this->setTableName('permissions');
    }
    public final function run($direction)
    {
      $list = ['Users', 'Permissions'];
      foreach ($list as $l)
      {
        $a = ['name' => $l];
        $this->db->insert($this->getTableName(), $a);
      }
    }
  }