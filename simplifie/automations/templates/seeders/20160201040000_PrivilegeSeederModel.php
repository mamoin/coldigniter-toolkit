<?php
  if(!defined('BASEPATH')) exit('Direct script access not allowed.');
  class PrivilegeSeederModel extends SeederModel
  {
    public function __construct()
    {
      $this->setTableName('privileges');
    }
    public final function run($direction)
    {
      $list = 
      [
        'Create', 
        'Read', 
        'Update', 
        'Delete'
      ];
      foreach($list as $l)
      {
        $a = ['name' => $l];
        $this->db->insert($this->getTableName(), $a);
      }
    }
  }