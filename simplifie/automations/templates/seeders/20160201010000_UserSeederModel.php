<?php
  if(!defined('BASEPATH')) exit('Direct script access not allowed.');
  class UserSeederModel extends SeederModel
  {
    public function __construct()
    {
      $this->setTableName('users');
    }
    public final function run($direction)
    {
      $a = 
      [
        'full_name' => 'Super Administrator',
        'email'     => 'superadmin@simplifie.net',
        'password'  => 'superadmin'
      ];
      $this->db->insert($this->getTableName(), $a);
    }
  }