<?php
  if(!defined('BASEPATH')) exit('Direct script access not allowed.');
  class RoleSeederModel extends SeederModel
  {
    public function __construct()
    {
      $this->setTableName('roles');
    }
    public final function run($direction)
    {
      $list = 
      [
        'Super Administrator', 
        'Administrator', 
        'Editor', 
        'Author',
        'Subscriber'
      ];
      foreach($list as $l)
      {
        $a = ['name' => $l];
        $this->db->insert($this->getTableName(), $a);
      }
    }
  }