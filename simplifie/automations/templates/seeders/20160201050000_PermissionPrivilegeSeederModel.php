<?php
  if(!defined('BASEPATH')) exit('Direct script access not allowed.');
  class PermissionPrivilegeSeederModel extends SeederModel
  {
    public function __construct()
    {
      $this->setTableName('permission_privileges');
    }
    public final function run($direction)
    {
      $privileges = 
      [
        'Create', 
        'Read', 
        'Update', 
        'Delete'
      ];
      $permissions = ['Users' => 'Permissions'];
      $user = ['full_name' => 'Super Administrator'];
      //
      $uId = $this->db->get_where
      (
        'users', $user
      )->row()->id;
      foreach ($permissions as $perm)
      {
        $permId = $this->db->get_where
        (
          'permissions', ['name' => $perm]
        )->row()->id;
        foreach ($privileges as $priv) 
        {
          $privId = $this->db->get_where
          (
            'privileges', ['name' => $priv]
          )->row()->id;
          $a = 
          [
            'user_id'        => $uId, 
            'permission_id'  => $permId,
            'privilege_id'   => $privId
          ];
          $this->db->insert($this->getTableName(), $a);
        }
      }
    }
  }