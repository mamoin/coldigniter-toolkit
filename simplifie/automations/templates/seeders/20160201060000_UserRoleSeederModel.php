<?php
  if(!defined('BASEPATH')) exit('Direct script access not allowed.');
  class UserRoleSeederModel extends SeederModel
  {
    public function __construct()
    {
      $this->setTableName('user_roles');
    }
    public final function run($direction)
    {
      $s = 'Super Administrator';
      $a = 
      [
        'user' => ['full_name' => $s],
        'role' => ['name'      => $s]
      ];
      $uId = $this->db->get_where('users', $a['user'])->row()->id;
      $rId = $this->db->get_where('roles', $a['role'])->row()->id;
      $a = ['user_id' => $uId, 'role_id' => $rId];
      $this->db->insert($this->getTableName(), $a);
    }
  }