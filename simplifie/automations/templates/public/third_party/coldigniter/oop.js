/**
  * OOP JS.
  * Enables OOP for JS.
  * @package  Utils.
  * @author  Mark N. Amoin
  * 
  */
/*
  Sample:
  function BaseClass()
  {
    this.sName;
    this.setName = function(name){this.sName = name;};
    this.getName = function(){return this.sName;};
    this.setName('BaseClass');
  }
  function ChildClass()
  {
    ChildClass.extends(BaseClass);
    this.showName = function()
    {
      console.log('Showing name:', this.getName());
    };
    BaseClass.call(this);
    this.setName.call(this, 'ChildClass');
  }
  //Usage:
  $(window).load(function()
  {
    function App()
    {
      this.aChildClasses;
      this.init = function()
      {
        this.aChildClasses = [];
        this.setListeners();
      };
      this.setListeners = function()
      {
        var r = this;
        $('button').on('click', function()
        {
          r.addChildClass(new ChildClass());
          r.addChildClass(new ChildClass());
          r.addChildClass(new ChildClass());
          r.showAllChildClassNames();
        });
      };
      this.addChildClass = function(childClass)
      {
        childClass.setName(Math.random() * 99);
        this.aChildClasses.push(childClass);
      };
      this.showAllChildClassNames = function()
      {
        var i = this.aChildClasses.length;
        for(var a = 0; a < i; a++)
        {
          this.aChildClasses[a].showName();
        }
      };
    }
    new App().init();
  });
*/
Function.prototype.extends = function(baseClass)
{
  this.prototype = Object.create(baseClass.prototype);
  this.prototype.contructor = this;
};