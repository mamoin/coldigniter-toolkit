<?php 
  //php index.php crud generate role id:int name:varchar
  
  if(!defined('BASEPATH')) 
    exit('No direct script access allowed');
  class Role extends CI_Controller 
  {
  	public function __construct()
  	{
  		parent::__construct();
      $this->load->model('rolemodel');
  	}
    public final function index()
    {
      $o = $this->rolemodel->index()->result();
      showView('roles/index', array('roles' => $o));
    }
    public final function create()
    {
      $a = array();
      if($this->input->post())
      {
        if($this->form_validation->run('role/create'))
        {
          $r = $this->rolemodel->create();
          if($r->num_rows())
          {
            $o = $r->row();
            redirect(site_url('role/update/' . $o->id));
          }
          else
          {
            $a['status'] = 'failed';
            $a['message'] = 'Error creating role.';
            showView('roles/create', $a);
          }
        }
        else
        {
          $a['status'] = 'failed';
          $a['message'] = validation_errors();
          showView('roles/create', $a);
        }
      }
      else
      {
        showView('roles/create', $a);
      }
    }
  	public final function read($id)
  	{
  		showView('roles/read', array('role' => $this->rolemodel->read($id)->row()));
  	}
  	public final function update($id = null)
    {
      $o = $this->rolemodel->read($id)->row();
      $a = array('role' => $o);
      if($this->input->post())
      {
        if($this->form_validation->run('role/update'))
        {
          $this->rolemodel->update();
          $o = $this->rolemodel->read($this->input->post('id'))->row();
          $a['role'] = $o;
          $a['status'] = 'success';
          $a['message'] = 'Successfully updated.';
          showView('roles/update', $a);
        }
        else
        {
          $a['status'] = 'failed';
          $a['message'] = validation_errors();
          showView('roles/update', $a);
        }
      }
      else
      {
        showView('roles/update', $a);
      }
    }
  	public final function delete($id, $format = 'html')
    {
      switch($format)
      {
        case 'html':
          $this->rolemodel->delete($id);
          redirect(site_url('role'));
        break;
        case 'json':
          showJsonView(array('role' => $this->rolemodel->delete($id)->row()));
        break;
      }
    }
  }