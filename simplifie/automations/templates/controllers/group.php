<?php 
  //php index.php crud generate group id:int name:varchar description:text
  
  if(!defined('BASEPATH')) 
    exit('No direct script access allowed');
  class Group extends CI_Controller 
  {
  	public function __construct()
  	{
  		parent::__construct();
      $this->load->model('groupmodel');
  	}
    public final function index()
    {
      $o = $this->groupmodel->index()->result();
      showView('groups/index', array('groups' => $o));
    }
    public final function create()
    {
      $a = array();
      if($this->input->post())
      {
        if($this->form_validation->run('group/create'))
        {
          $r = $this->groupmodel->create();
          if($r->num_rows())
          {
            $o = $r->row();
            redirect(site_url('group/update/' . $o->id));
          }
          else
          {
            $a['status'] = 'failed';
            $a['message'] = 'Error creating group.';
            showView('groups/create', $a);
          }
        }
        else
        {
          $a['status'] = 'failed';
          $a['message'] = validation_errors();
          showView('groups/create', $a);
        }
      }
      else
      {
        showView('groups/create', $a);
      }
    }
  	public final function read($id)
  	{
  		showView('groups/read', array('group' => $this->groupmodel->read($id)->row()));
  	}
  	public final function update($id = null)
    {
      $o = $this->groupmodel->read($id)->row();
      $a = array('group' => $o);
      if($this->input->post())
      {
        if($this->form_validation->run('group/update'))
        {
          $this->groupmodel->update();
          $o = $this->groupmodel->read($this->input->post('id'))->row();
          $a['group'] = $o;
          $a['status'] = 'success';
          $a['message'] = 'Successfully updated.';
          showView('groups/update', $a);
        }
        else
        {
          $a['status'] = 'failed';
          $a['message'] = validation_errors();
          showView('groups/update', $a);
        }
      }
      else
      {
        showView('groups/update', $a);
      }
    }
  	public final function delete($id, $format = 'html')
    {
      switch($format)
      {
        case 'html':
          $this->groupmodel->delete($id);
          redirect(site_url('group'));
        break;
        case 'json':
          showJsonView(array('group' => $this->groupmodel->delete($id)->row()));
        break;
      }
    }
  }