<?php
  if (!defined('BASEPATH')) exit('No direct script access allowed');
  /**
   * Class Crud.
   * Generates CRUD files and places them in 
   * application/third_party/coldigniter/automations/cruds folder,
   * which you can then use to replace your old files.
   * @package   CodeIgniter
   * @subpackage  Libraries
   * @author Mark N. Amoin
   */
  class Crud extends CI_Controller
  {
    //BUG: phpDocumentor issues an error when using CONST.
    /**
     * @var string Relative base path to files needed by this class.
     * @access private
     */ 
    private $GENERATE_PATH;
    
    /**
     * @var string Path for the view folder.
     * @access private
     */ 
    private $sCrudViewFldr;

    private $sDate;

    /** Constructor. */
    public function __construct() 
    {
      parent::__construct();
      $this->load->helper( 'inflector' );
      $this->load->library( 'parser' );
      $this->load->helper( 'file' );
      //No trailing slash.
      $this->GENERATE_PATH = APPPATH . 'third_party/coldigniter/automations/';
      $s = $this->GENERATE_PATH;
      if ( !file_exists( $s ) ) 
      {
        mkdir( $s );
      }
    }
    /**
     * Generates CRUD files.
     * CLI usage: 
     * <code>php index.php crud generate student id:int full_name:varchar description:text enabled:boolean birth_date:datetime avatar:file gender:enum:Male-Female user:ref</code>
     * @access public
     */
    public final function generate() 
    {
      if ( $this->input->is_cli_request() ) 
      {
        $sTmplPath = APPPATH . 'third_party/coldigniter/automations/templates';
        $this->load->add_package_path($sTmplPath);
        $params = $_SERVER['argv'];
        $fields = [];
        $len = $_SERVER['argc'];
        for ( $a = 4; $a < $len; $a++ ) 
        {
          array_push( $fields, $params[$a] );
        }
        $entity = $params[3];
        echo "Generating...\n";
        //Create initial CRUD dir.
        //Use date for versioning.
        $this->sDate = date( 'YmdHis' );
        $this->sCrudViewFldr = $this->GENERATE_PATH . 'cruds/' . plural( $entity ) . '_' . $this->sDate;
        //
        if ( !file_exists( $this->sCrudViewFldr ) ) 
        {
          mkdir( $this->sCrudViewFldr );
          $this->echoFileCreated( 'New directory', $this->sCrudViewFldr );
        }
        //Record last executed CLI command in the controller.
        $sCmd = 'php';
        foreach ($params as $p) 
        {
          $sCmd .= ' ' . $p;
        }
        //
        $this->createCrudCtrl( $sCmd, $entity );
        $this->createCrudMdl( $entity, $fields );
        $this->createCrudViews( $entity, $fields );
        $this->createCrudValidation( $entity );
        $this->createTable( $entity, $fields );
        $this->createMigration( $entity, $fields );
        $this->load->remove_package_path($sTmplPath);
        echo "Done.\n";
      }
      PHP_EOL;
      exit( 0 );
    }
    /**
     * Creates the migration files.
     * @param  [type] $entity [description]
     * @param  [type] $fields [description]
     * @return [type]         [description]
     */
    private final function createMigration($entity, $fields)
    {
      $filename = $this->sCrudViewFldr . '/' . $this->sDate . '_' . str_replace( '_', '', $entity ) . 'model.php';
      $contents = $this->parser->parse
      (
        'migration', 
        ['entity' => $entity, 'fields' => $fields], 
        TRUE
      );
      write_file($filename, $contents);
      $this->echoFileCreated('Migration', $filename);
    }
    /**
     * Creates table file.
     * @param string $entity description.
     * @param array $fields description.
     * @access private
     */
    private final function createTable( $entity, $fields ) 
    {
      $aDefVals = array
      (
        'int' => 'INT',
        'varchar' => 'VARCHAR(255)',
        'file' => 'VARCHAR(255)',
        'float' => 'FLOAT(11)', 
        'datetime' => 'DATETIME',
        'text' => 'TEXT',
        'boolean' => 'TINYINT(1)',
        'enum' => 'ENUM',
        'ref' => '' //Index relationship.
      );
      $entity = plural( $entity );
      $contents = "CREATE TABLE $entity(";
      foreach ( $fields as $f ) 
      {
        $l = explode( ':', $f );
        $key = $l[0];
        $val = $l[1];
        if('enum' === $val)
        {
          $enumParams = str_replace('-', "','", $l[2]);
          $contents .= $key . ' ' . $aDefVals[$val] . '(\'' . $enumParams . '\')';
        }
        else 
        {
          //Relations.
          if('ref' === $val)
          {
            $contents .= $key . "_id INT NOT NULL, \n" . 
              'FOREIGN KEY(' . $key . 
              '_id) REFERENCES ' . plural($key) . 
              '(id) ON UPDATE CASCADE ON DELETE CASCADE';
          }
          else
          {
            $contents .= $key . ' ' . $aDefVals[$val];
          }
        }
        if($val != 'ref') $contents .= ' NOT NULL';
        //
        if ( $key == 'id' ) 
        {
          $contents .= ' AUTO_INCREMENT';
        }
        $contents .= ",\n";
      }
      $contents .=  'PRIMARY KEY (id))';
      $filename = $this->sCrudViewFldr . '/' . $entity . '_table';
      write_file( $filename . '.sql', $contents );
      $this->echoFileCreated( 'Database table', $filename );
    }

    /**
     * Creates controller file.
     * @param string $command description.
     * @param string $entity description.
     * @access private
     */
    private final function createCrudCtrl( $command, $entity ) 
    {
      $filename = $this->sCrudViewFldr . '/' . str_replace( '_', '', $entity ) . '.php';
      $contents = $this->parser->parse
      ( 
        'controller', 
        [ 'entity' => $entity, 'command' => $command ], 
        TRUE 
      );
      write_file( $filename, $contents );
      $this->echoFileCreated( 'Controller', $filename );
    }

    /**
     * Creates model file.
     * @param string $entity description.
     * @param array $fields description.
     * @access private
     */
    private final function createCrudMdl( $entity, $fields = array() ) {
      $filename = $this->sCrudViewFldr . '/' . str_replace( '_', '', $entity ) . 'model.php';
      $aFiles = [];
      foreach ( $fields as $f ) 
      {
        $l = explode( ':', $f );
        $key = $l[0];
        $val = $l[1];
        if('file' === $val)
        {
          array_push($aFiles, [$key, $val]);
        }
      }
      $contents = $this->parser->parse
      ( 
        'model', 
        [ 'entity' => $entity, 'files' => $aFiles ], 
        TRUE 
      );
      write_file( $filename, $contents );
      $this->echoFileCreated( 'Model', $filename );
    }

    /**
     * Creates view files.
     * @param string $entity description.
     * @param array $fields description.
     * @access private
     */
    private final function createCrudViews( $entity, $fields = array() ) 
    {
      $fldr = plural( $entity );
      $a = [];
      foreach ( $fields as $f ) 
      {
        $keyVal = explode( ':', $f );
        $key = $keyVal[0];
        $tmp = NULL;
        switch ( $keyVal[1] ) 
        {
          case 'int':
          case 'varchar':
          case 'datetime':
          case 'float':
            $tmp = array( 'name' => $key, 'field' => $this->toFormField( 'text', $entity, $key ) );
            break;
          case 'text':
            $tmp = array( 'name' => $key, 'field' => $this->toFormField( 'textarea', $entity, $key ) );
            break;
          case 'boolean':
            $tmp = array( 'name' => $key, 'field' => $this->toFormField( 'checkbox', $entity, $key ) );
            break;
          case 'file':
            $tmp = array( 'name' => $key, 'field' => $this->toFormField( 'file', $entity, $key ) );
            break;
          case 'enum':
            $tmp = array( 'name' => $key, 'field' => $this->toFormField( 'enum', $entity, $key ) );
            break;
        }
        array_push( $a, $tmp );
      }
      $this->createView( $entity, $a, 'index' );
      $this->createView( $entity, $a, 'create' );
      $this->createView( $entity, $a, 'read' );
      //
      //Re-create the fields again this time with set_values() during form validation.
      $a = [];
      foreach ( $fields as $f ) 
      {
        $keyVal = explode( ':', $f );
        $key = $keyVal[0];
        $tmp = NULL;
        switch ( $keyVal[1] ) 
        {
          case 'int':
          case 'varchar':
          case 'datetime':
          case 'float':
            $tmp = array( 'name' => $key, 'field' => $this->toFormField( 'text', $entity, $key, true ) );
            break;
          case 'text':
            $tmp = array( 'name' => $key, 'field' => $this->toFormField( 'textarea', $entity, $key, true ) );
            break;
          case 'boolean':
            $tmp = array( 'name' => $key, 'field' => $this->toFormField( 'checkbox', $entity, $key, true ) );
            break;
          case 'file':
            $tmp = array( 'name' => $key, 'field' => $this->toFormField( 'file', $entity, $key, true ) );
            break;
          case 'enum':
            $tmp = array( 'name' => $key, 'field' => $this->toFormField( 'enum', $entity, $key, true ) );
            break;
        }
        array_push( $a, $tmp );
      }
      $this->createView( $entity, $a, 'update' );
      //Delete will be using JS in the index view.
    }

    /**
     * Creates form fields.
     * @param string $type description.
     * @param array $entity description.
     * @param string $key description.
     * @param bool $isUpdate description.
     * @access private
     */
    private final function toFormField( $type, $entity, $key, $isUpdate = false ) 
    {
      $cml = camelize( $entity );
      $value = $isUpdate ? '<?php echo set_value(\'' . $key . '\', $' . $cml . '->' . $key . '); ?>' : '';
      $s = NULL;
      switch ( $type ) 
      {
        case 'text':
          $s = $isUpdate ?
            '<input type="text" name="' . $key . '" value="' . $value . '" />' :
            '<input type="text" name="' . $key . '" />';
          break;
        case 'textarea':
          $s = $isUpdate ?
            '<textarea name="' . $key . '">' . $value . '</textarea>' :
            '<textarea name="' . $key . '"></textarea>';
          break;
        case 'checkbox':
          $sChecked = '<?php echo set_value(\'' . $key . '\',  $' . $cml . '->' . $key . '); ?>';
          $s = $isUpdate ?
            '<input type="checkbox" name="' . $key . '" checked="' . $sChecked . '" />' :
            '<input type="checkbox" name="' . $key . '" />';
          break;
        case 'file':
          $s = '<input type="file" name="' . $key . '" />';
        break;
        case 'enum':
          $sSelected = 'set_value(\'' . $key . '\',  $' . $cml . '->' . $key . ')';
          $s = $isUpdate ?
            '<?php echo form_dropdown(\'' . $key . '\', $' . plural($key) . ', ' . $sSelected . '); ?>' :
            '<?php echo form_dropdown(\'' . $key . '\', $' . plural($key) . '); ?>';
        break;
      }
      return $s;
    }

    /**
     * Creates view file.
     * @param array $entity description.
     * @param array $fields description.
     * @param string $type description.
     * @access private
     */
    private final function createView( $entity, $fields, $type ) 
    {
      $filename = $this->sCrudViewFldr . '/' . $type . '.php';
      $contents = $this->load->view
      (
        $type,
        [ 'entity' => $entity, 'fields' => $fields ],
        TRUE
      );
      write_file( $filename, $contents );
      $this->echoFileCreated( 'View', $filename );
    }

    /**
     * Creates validation file.
     * @param string $entity description.
     * @access private
     */
    private final function createCrudValidation( $entity ) 
    {
      $contents = $this->load->view
      (
        'form_validation',
        [ 'entity' => str_replace( '_', '', $entity ) ],
        TRUE
      );
      $filename = $this->sCrudViewFldr . '/form_validation.php';
      write_file( $filename, $contents );
      $this->echoFileCreated( 'Validation', $filename );
    }

    /**
     * Displays a confirmation file created message in the CLI.
     * @param string $type description.
     * @param string $filename description.
     * @access private
     */
    private final function echoFileCreated
    (
      $type,
      $filename
    ) 
    {
      echo "$type $filename created.\n";
    }
  }