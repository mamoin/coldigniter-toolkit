<?php 
  //php index.php crud generate permission id:int name:varchar
  
  if(!defined('BASEPATH')) 
    exit('No direct script access allowed');
  class Permission extends CI_Controller 
  {
  	public function __construct()
  	{
  		parent::__construct();
      $this->load->model('permissionmodel');
  	}
    public final function index()
    {
      $o = $this->permissionmodel->index()->result();
      showView('permissions/index', array('permissions' => $o));
    }
    public final function create()
    {
      $a = array();
      if($this->input->post())
      {
        if($this->form_validation->run('permission/create'))
        {
          $r = $this->permissionmodel->create();
          if($r->num_rows())
          {
            $o = $r->row();
            redirect(site_url('permission/update/' . $o->id));
          }
          else
          {
            $a['status'] = 'failed';
            $a['message'] = 'Error creating permission.';
            showView('permissions/create', $a);
          }
        }
        else
        {
          $a['status'] = 'failed';
          $a['message'] = validation_errors();
          showView('permissions/create', $a);
        }
      }
      else
      {
        showView('permissions/create', $a);
      }
    }
  	public final function read($id)
  	{
  		showView('permissions/read', array('permission' => $this->permissionmodel->read($id)->row()));
  	}
  	public final function update($id = null)
    {
      $o = $this->permissionmodel->read($id)->row();
      $a = array('permission' => $o);
      if($this->input->post())
      {
        if($this->form_validation->run('permission/update'))
        {
          $this->permissionmodel->update();
          $o = $this->permissionmodel->read($this->input->post('id'))->row();
          $a['permission'] = $o;
          $a['status'] = 'success';
          $a['message'] = 'Successfully updated.';
          showView('permissions/update', $a);
        }
        else
        {
          $a['status'] = 'failed';
          $a['message'] = validation_errors();
          showView('permissions/update', $a);
        }
      }
      else
      {
        showView('permissions/update', $a);
      }
    }
  	public final function delete($id, $format = 'html')
    {
      switch($format)
      {
        case 'html':
          $this->permissionmodel->delete($id);
          redirect(site_url('permission'));
        break;
        case 'json':
          showJsonView(array('permission' => $this->permissionmodel->delete($id)->row()));
        break;
      }
    }
  }