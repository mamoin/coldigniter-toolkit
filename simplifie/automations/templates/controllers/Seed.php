<?php
  use simplifie\helpers\ViewHelper;
  use simplifie\helpers\AuthHelper;
  if (!defined('BASEPATH')) exit('No direct script access allowed');
  class Seed extends CI_Controller
  {
    public function __construct()
    {
      parent::__construct();
      $this->load->helper('url');
      $this->load->library('session');
    }
    public final function index()
    {
      $i = $this->input;
      $success = NULL;
      $params = ['successful_seeders' => NULL];
      if('post' === $i->method())
      {
        AuthHelper::filterIsInternalAdmin('seed');
        $this->load->library('seeder');
        $this->load->library('typography');
        $a = $i->post('seeders');
        $bTruncateAll = 'on' === $i->post('truncate_all');
        $params['truncateAll'] = $bTruncateAll;
        if(count($a) > 0) $success = TRUE;
        $this->seeder->setSeeders($a);
        $this->seeder->run(
          NULL,
          NULL,
          $params['successfulSeeders'],
          $bTruncateAll
        );
      }
      $params['title'] = 'Seeders';
      $params['success'] = $success;
      //
      $this->load->helper('inflector');
      $this->load->helper('directory');
      $this->load->helper('form');
      $this->load->helper('url');
      ViewHelper::showInternal('seed', $params);
    }
  }