<?php
  if (!defined('BASEPATH')) exit('No direct script access allowed');
  /**
   * An extendable base class that provides
   * better access to assets linked to ViewHelper.
   */
  class Base extends CI_Controller
  {
    private $aJs;
    private $aCss;
    public function __construct()
    {
      parent::__construct();
      $this->aJs = [];
      $this->aCss = [];
    }
    protected final function getJs()
    {
      return $this->aJs;
    }
    protected final function setJs($list)
    {
      $this->aJs = $list;
    }
    protected final function getCss()
    {
      return $this->aCss;
    }
    protected final function setCss($list)
    {
      $this->aCss = $list;
    }
  }