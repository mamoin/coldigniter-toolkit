<?php
  use simplifie\helpers\AuthHelper;
  use simplifie\helpers\ViewHelper;
  if (!defined('BASEPATH')) exit('No direct script access allowed');
  class Migrate extends CI_Controller
  {
    public function __construct()
    {
      parent::__construct();
      $this->config->load('migration', TRUE);
      $this->load->library('migration');
      $this->load->helper('form');
      $this->load->helper('inflector');
      $this->load->helper('url');
      $this->load->library('session');
    }
    public final function index()
    {
      if($this->input->method() === 'post')
      {
        AuthHelper::filterIsInternalAdmin('migrate');
        $version = $this->input->post('version');
        if(!$this->migration->version($version))
        {
          $this->show_view('Error. ' . $this->migration->error_string());
        }
        else
        {
          $this->show_view('Migration was successful!');
        }
      }
      else
      {
        $this->show_view();
      }
    }
    public final function current()
    {
      $this->migration->current();
      redirect(site_url('migrate'));
    }
    private final function show_view($message = NULL)
    {
      ViewHelper::showInternal('migrate', [
        'title'       => 'Migration',
        'message'      => $message,
        'ideal'        => $this->config->item('migration_version', 'migration'),
        'current'      => $this->get_current_version(),
        'migrations'   => $this->get_migrations()
      ]);
    }
    private final function get_current_version()
    {
      $r = $this->db->get('migrations')->row();
      return $r ? $r->version : 0;
    }
    private final function get_migrations()
    {
      $tmp = $this->migration->find_migrations();
      $migrations = [];
      foreach($tmp as $t)
      {
        //Remove path.
        $s = str_replace('app/migrations/', '', $t);
        //Dropdown readable and indices.
        //TODO: Concat --selected.
        $h = humanize(basename($s, '.php'));
        $i = filter_var($h, FILTER_SANITIZE_NUMBER_INT) . '';
        $migrations[$i] = $h;
      }
      return $migrations;
    }
  }