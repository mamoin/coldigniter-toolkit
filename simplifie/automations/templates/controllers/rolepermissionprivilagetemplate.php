<?php 
  //php index.php crud generate rolepermissionprivilagetemplate id:int privilage:ref description:text enabled:boolean role_permission_template:ref
  
  if(!defined('BASEPATH')) 
    exit('No direct script access allowed');
  class Rolepermissionprivilagetemplate extends CI_Controller 
  {
  	public function __construct()
  	{
  		parent::__construct();
      $this->load->model('rolepermissionprivilagetemplatemodel');
  	}
    public final function index()
    {
      $o = $this->rolepermissionprivilagetemplatemodel->index()->result();
      showView('rolepermissionprivilagetemplates/index', array('rolepermissionprivilagetemplates' => $o));
    }
    public final function create()
    {
      $a = array();
      if($this->input->post())
      {
        if($this->form_validation->run('rolepermissionprivilagetemplate/create'))
        {
          $r = $this->rolepermissionprivilagetemplatemodel->create();
          if($r->num_rows())
          {
            $o = $r->row();
            redirect(site_url('rolepermissionprivilagetemplate/update/' . $o->id));
          }
          else
          {
            $a['status'] = 'failed';
            $a['message'] = 'Error creating rolepermissionprivilagetemplate.';
            showView('rolepermissionprivilagetemplates/create', $a);
          }
        }
        else
        {
          $a['status'] = 'failed';
          $a['message'] = validation_errors();
          showView('rolepermissionprivilagetemplates/create', $a);
        }
      }
      else
      {
        showView('rolepermissionprivilagetemplates/create', $a);
      }
    }
  	public final function read($id)
  	{
  		showView('rolepermissionprivilagetemplates/read', array('rolepermissionprivilagetemplate' => $this->rolepermissionprivilagetemplatemodel->read($id)->row()));
  	}
  	public final function update($id = null)
    {
      $o = $this->rolepermissionprivilagetemplatemodel->read($id)->row();
      $a = array('rolepermissionprivilagetemplate' => $o);
      if($this->input->post())
      {
        if($this->form_validation->run('rolepermissionprivilagetemplate/update'))
        {
          $this->rolepermissionprivilagetemplatemodel->update();
          $o = $this->rolepermissionprivilagetemplatemodel->read($this->input->post('id'))->row();
          $a['rolepermissionprivilagetemplate'] = $o;
          $a['status'] = 'success';
          $a['message'] = 'Successfully updated.';
          showView('rolepermissionprivilagetemplates/update', $a);
        }
        else
        {
          $a['status'] = 'failed';
          $a['message'] = validation_errors();
          showView('rolepermissionprivilagetemplates/update', $a);
        }
      }
      else
      {
        showView('rolepermissionprivilagetemplates/update', $a);
      }
    }
  	public final function delete($id, $format = 'html')
    {
      switch($format)
      {
        case 'html':
          $this->rolepermissionprivilagetemplatemodel->delete($id);
          redirect(site_url('rolepermissionprivilagetemplate'));
        break;
        case 'json':
          showJsonView(array('rolepermissionprivilagetemplate' => $this->rolepermissionprivilagetemplatemodel->delete($id)->row()));
        break;
      }
    }
  }