<?php 
  //php index.php crud generate rolepermissiontemplate id:int permission:ref description:text enabled:boolean role_template:ref
  
  if(!defined('BASEPATH')) 
    exit('No direct script access allowed');
  class Rolepermissiontemplate extends CI_Controller 
  {
  	public function __construct()
  	{
  		parent::__construct();
      $this->load->model('rolepermissiontemplatemodel');
  	}
    public final function index()
    {
      $o = $this->rolepermissiontemplatemodel->index()->result();
      showView('rolepermissiontemplates/index', array('rolepermissiontemplates' => $o));
    }
    public final function create()
    {
      $a = array();
      if($this->input->post())
      {
        if($this->form_validation->run('rolepermissiontemplate/create'))
        {
          $r = $this->rolepermissiontemplatemodel->create();
          if($r->num_rows())
          {
            $o = $r->row();
            redirect(site_url('rolepermissiontemplate/update/' . $o->id));
          }
          else
          {
            $a['status'] = 'failed';
            $a['message'] = 'Error creating rolepermissiontemplate.';
            showView('rolepermissiontemplates/create', $a);
          }
        }
        else
        {
          $a['status'] = 'failed';
          $a['message'] = validation_errors();
          showView('rolepermissiontemplates/create', $a);
        }
      }
      else
      {
        showView('rolepermissiontemplates/create', $a);
      }
    }
  	public final function read($id)
  	{
  		showView('rolepermissiontemplates/read', array('rolepermissiontemplate' => $this->rolepermissiontemplatemodel->read($id)->row()));
  	}
  	public final function update($id = null)
    {
      $o = $this->rolepermissiontemplatemodel->read($id)->row();
      $a = array('rolepermissiontemplate' => $o);
      if($this->input->post())
      {
        if($this->form_validation->run('rolepermissiontemplate/update'))
        {
          $this->rolepermissiontemplatemodel->update();
          $o = $this->rolepermissiontemplatemodel->read($this->input->post('id'))->row();
          $a['rolepermissiontemplate'] = $o;
          $a['status'] = 'success';
          $a['message'] = 'Successfully updated.';
          showView('rolepermissiontemplates/update', $a);
        }
        else
        {
          $a['status'] = 'failed';
          $a['message'] = validation_errors();
          showView('rolepermissiontemplates/update', $a);
        }
      }
      else
      {
        showView('rolepermissiontemplates/update', $a);
      }
    }
  	public final function delete($id, $format = 'html')
    {
      switch($format)
      {
        case 'html':
          $this->rolepermissiontemplatemodel->delete($id);
          redirect(site_url('rolepermissiontemplate'));
        break;
        case 'json':
          showJsonView(array('rolepermissiontemplate' => $this->rolepermissiontemplatemodel->delete($id)->row()));
        break;
      }
    }
  }