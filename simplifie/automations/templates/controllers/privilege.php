<?php 
  //php index.php crud generate privilege id:int name:varchar
  
  if(!defined('BASEPATH')) 
    exit('No direct script access allowed');
  class Privilege extends CI_Controller 
  {
  	public function __construct()
  	{
  		parent::__construct();
      $this->load->model('privilegemodel');
  	}
    public final function index()
    {
      $o = $this->privilegemodel->index()->result();
      showView('privileges/index', array('privileges' => $o));
    }
    public final function create()
    {
      $a = array();
      if($this->input->post())
      {
        if($this->form_validation->run('privilege/create'))
        {
          $r = $this->privilegemodel->create();
          if($r->num_rows())
          {
            $o = $r->row();
            redirect(site_url('privilege/update/' . $o->id));
          }
          else
          {
            $a['status'] = 'failed';
            $a['message'] = 'Error creating privilege.';
            showView('privileges/create', $a);
          }
        }
        else
        {
          $a['status'] = 'failed';
          $a['message'] = validation_errors();
          showView('privileges/create', $a);
        }
      }
      else
      {
        showView('privileges/create', $a);
      }
    }
  	public final function read($id)
  	{
  		showView('privileges/read', array('privilege' => $this->privilegemodel->read($id)->row()));
  	}
  	public final function update($id = null)
    {
      $o = $this->privilegemodel->read($id)->row();
      $a = array('privilege' => $o);
      if($this->input->post())
      {
        if($this->form_validation->run('privilege/update'))
        {
          $this->privilegemodel->update();
          $o = $this->privilegemodel->read($this->input->post('id'))->row();
          $a['privilege'] = $o;
          $a['status'] = 'success';
          $a['message'] = 'Successfully updated.';
          showView('privileges/update', $a);
        }
        else
        {
          $a['status'] = 'failed';
          $a['message'] = validation_errors();
          showView('privileges/update', $a);
        }
      }
      else
      {
        showView('privileges/update', $a);
      }
    }
  	public final function delete($id, $format = 'html')
    {
      switch($format)
      {
        case 'html':
          $this->privilegemodel->delete($id);
          redirect(site_url('privilege'));
        break;
        case 'json':
          showJsonView(array('privilege' => $this->privilegemodel->delete($id)->row()));
        break;
      }
    }
  }