<?php 
  //php index.php crud generate user id:int full_name:varchar email:varchar password:varchar gender:enum:Male-Female enabled:boolean
  
  if(!defined('BASEPATH')) 
    exit('No direct script access allowed');
  class User extends CI_Controller 
  {
  	public function __construct()
  	{
  		parent::__construct();
      $this->load->model('usermodel');
  	}
    public final function index()
    {
      $o = $this->usermodel->index()->result();
      showView('users/index', array('users' => $o));
    }
    public final function create()
    {
      $a = array();
      if($this->input->post())
      {
        if($this->form_validation->run('user/create'))
        {
          $r = $this->usermodel->create();
          if($r->num_rows())
          {
            $o = $r->row();
            redirect(site_url('user/update/' . $o->id));
          }
          else
          {
            $a['status'] = 'failed';
            $a['message'] = 'Error creating user.';
            showView('users/create', $a);
          }
        }
        else
        {
          $a['status'] = 'failed';
          $a['message'] = validation_errors();
          showView('users/create', $a);
        }
      }
      else
      {
        showView('users/create', $a);
      }
    }
  	public final function read($id)
  	{
  		showView('users/read', array('user' => $this->usermodel->read($id)->row()));
  	}
  	public final function update($id = null)
    {
      $o = $this->usermodel->read($id)->row();
      $a = array('user' => $o);
      if($this->input->post())
      {
        if($this->form_validation->run('user/update'))
        {
          $this->usermodel->update();
          $o = $this->usermodel->read($this->input->post('id'))->row();
          $a['user'] = $o;
          $a['status'] = 'success';
          $a['message'] = 'Successfully updated.';
          showView('users/update', $a);
        }
        else
        {
          $a['status'] = 'failed';
          $a['message'] = validation_errors();
          showView('users/update', $a);
        }
      }
      else
      {
        showView('users/update', $a);
      }
    }
  	public final function delete($id, $format = 'html')
    {
      switch($format)
      {
        case 'html':
          $this->usermodel->delete($id);
          redirect(site_url('user'));
        break;
        case 'json':
          showJsonView(array('user' => $this->usermodel->delete($id)->row()));
        break;
      }
    }
  }