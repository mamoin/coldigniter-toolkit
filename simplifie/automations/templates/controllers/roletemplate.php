<?php 
  //php index.php crud generate roletemplate id:int name:string description:text enabled:boolean created_by_user:ref
  
  if(!defined('BASEPATH')) 
    exit('No direct script access allowed');
  class Roletemplate extends CI_Controller 
  {
  	public function __construct()
  	{
  		parent::__construct();
      $this->load->model('roletemplatemodel');
  	}
    public final function index()
    {
      $o = $this->roletemplatemodel->index()->result();
      showView('roletemplates/index', array('roletemplates' => $o));
    }
    public final function create()
    {
      $a = array();
      if($this->input->post())
      {
        if($this->form_validation->run('roletemplate/create'))
        {
          $r = $this->roletemplatemodel->create();
          if($r->num_rows())
          {
            $o = $r->row();
            redirect(site_url('roletemplate/update/' . $o->id));
          }
          else
          {
            $a['status'] = 'failed';
            $a['message'] = 'Error creating roletemplate.';
            showView('roletemplates/create', $a);
          }
        }
        else
        {
          $a['status'] = 'failed';
          $a['message'] = validation_errors();
          showView('roletemplates/create', $a);
        }
      }
      else
      {
        showView('roletemplates/create', $a);
      }
    }
  	public final function read($id)
  	{
  		showView('roletemplates/read', array('roletemplate' => $this->roletemplatemodel->read($id)->row()));
  	}
  	public final function update($id = null)
    {
      $o = $this->roletemplatemodel->read($id)->row();
      $a = array('roletemplate' => $o);
      if($this->input->post())
      {
        if($this->form_validation->run('roletemplate/update'))
        {
          $this->roletemplatemodel->update();
          $o = $this->roletemplatemodel->read($this->input->post('id'))->row();
          $a['roletemplate'] = $o;
          $a['status'] = 'success';
          $a['message'] = 'Successfully updated.';
          showView('roletemplates/update', $a);
        }
        else
        {
          $a['status'] = 'failed';
          $a['message'] = validation_errors();
          showView('roletemplates/update', $a);
        }
      }
      else
      {
        showView('roletemplates/update', $a);
      }
    }
  	public final function delete($id, $format = 'html')
    {
      switch($format)
      {
        case 'html':
          $this->roletemplatemodel->delete($id);
          redirect(site_url('roletemplate'));
        break;
        case 'json':
          showJsonView(array('roletemplate' => $this->roletemplatemodel->delete($id)->row()));
        break;
      }
    }
  }