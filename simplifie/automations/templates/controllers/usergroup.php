<?php 
  //php index.php crud generate user_group id:int user_id:ref group_id:ref
  
  if(!defined('BASEPATH')) 
    exit('No direct script access allowed');
  class UserGroup extends CI_Controller 
  {
  	public function __construct()
  	{
  		parent::__construct();
      $this->load->model('usergroupmodel');
  	}
    public final function index()
    {
      $o = $this->usergroupmodel->index()->result();
      showView('user_groups/index', array('userGroups' => $o));
    }
    public final function create()
    {
      $a = array();
      if($this->input->post())
      {
        if($this->form_validation->run('user_group/create'))
        {
          $r = $this->usergroupmodel->create();
          if($r->num_rows())
          {
            $o = $r->row();
            redirect(site_url('usergroup/update/' . $o->id));
          }
          else
          {
            $a['status'] = 'failed';
            $a['message'] = 'Error creating user_group.';
            showView('user_groups/create', $a);
          }
        }
        else
        {
          $a['status'] = 'failed';
          $a['message'] = validation_errors();
          showView('user_groups/create', $a);
        }
      }
      else
      {
        showView('user_groups/create', $a);
      }
    }
  	public final function read($id)
  	{
  		showView('user_groups/read', array('userGroup' => $this->usergroupmodel->read($id)->row()));
  	}
  	public final function update($id = null)
    {
      $o = $this->usergroupmodel->read($id)->row();
      $a = array('userGroup' => $o);
      if($this->input->post())
      {
        if($this->form_validation->run('user_group/update'))
        {
          $this->usergroupmodel->update();
          $o = $this->usergroupmodel->read($this->input->post('id'))->row();
          $a['userGroup'] = $o;
          $a['status'] = 'success';
          $a['message'] = 'Successfully updated.';
          showView('user_groups/update', $a);
        }
        else
        {
          $a['status'] = 'failed';
          $a['message'] = validation_errors();
          showView('user_groups/update', $a);
        }
      }
      else
      {
        showView('user_groups/update', $a);
      }
    }
  	public final function delete($id, $format = 'html')
    {
      switch($format)
      {
        case 'html':
          $this->usergroupmodel->delete($id);
          redirect(site_url('usergroup'));
        break;
        case 'json':
          showJsonView(array('userGroup' => $this->usergroupmodel->delete($id)->row()));
        break;
      }
    }
  }