<div id="group" class="read row">
  <div class="small-12 medium-12 large-12 columns">
    <h4>Group</h4>
        <div class="row">
      <div class="small-12 medium-12 large-12 columns">  
          Id: <?php echo $group->id; ?>
</div>
      <div class="small-12 medium-12 large-12 columns">  
          Name: <?php echo $group->name; ?>
</div>
      <div class="small-12 medium-12 large-12 columns">  
          Description: <?php echo $group->description; ?>
</div>
    </div>
    <div class="row">
      <div class="small-12 medium-12 large-12 columns">
        <a href="<?php echo site_url('group'); ?>" class="button small alert">Back</a>
        <a href="<?php echo site_url('group/update/' . $group->id); ?>" class="button small">Update</a>
      </div>
    </div>
  </div>
</div>