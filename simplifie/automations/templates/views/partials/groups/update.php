<div id="group" class="update row">
  <div class="small-12 medium-12 large-12 columns">
    <h4>Update Group</h4>
        <?php 
        if(isset($status))
        {
          echo $this->load->view
          (
            'commons/partials/header_messages', 
            array('status' => $status), 
            true
          );
        }
        echo form_open('group/update'); 
      ?>      
      <div class="row">  
          <div class="small-12 medium-12 large-12 columns">
            Id: <input type="text" name="id" value="<?php echo set_value('id', $group->id); ?>" />        
        </div>
          <div class="small-12 medium-12 large-12 columns">
            Name: <input type="text" name="name" value="<?php echo set_value('name', $group->name); ?>" />        
        </div>
          <div class="small-12 medium-12 large-12 columns">
            Description: <textarea name="description"><?php echo set_value('description', $group->description); ?></textarea>        
        </div>
        </div>
      <div class="row">
        <div class="small-12 medium-12 large-12 columns">
          <a href="<?php echo site_url('group/read/'  . $group->id); ?>" class="button small alert">Back</a>
          <button class="button small">Update</button>
        </div>
      </div>
    </form>
  </div>
</div>