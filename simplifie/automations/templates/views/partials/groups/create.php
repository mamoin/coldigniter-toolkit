<div id="group" class="create row">
  <div class="small-12 medium-12 large-12 columns">
    <h4>New Group</h4>
        <?php 
        if(isset($status))
        {
          echo $this->load->view
          (
            'commons/partials/header_messages', 
            array('status' => $status), 
            true
          );
        }
        echo form_open('group/create'); 
      ?>
      <div class="row">
          <div class="small-12 medium-12 large-12 columns">        
            Id: <input type="text" name="id" />        
        </div>  
          <div class="small-12 medium-12 large-12 columns">        
            Name: <input type="text" name="name" />        
        </div>  
          <div class="small-12 medium-12 large-12 columns">        
            Description: <textarea name="description"></textarea>        
        </div>  
            <div class="small-12 medium-12 large-12 columns">
          <a href="<?php echo site_url('group'); ?>" class="button small alert">Cancel</a>
          <button class="button small">Create</button>
        </div>
      </div>
    </form>
  </div>
</div>