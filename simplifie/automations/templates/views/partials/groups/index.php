<div id="group" class="index row">
	<div class="small-12 medium-12 large-12 columns">
			<h4>Groups</h4>
	  	<a href="<?php echo site_url('group/create'); ?>" class="button small">New Group</a>
		
	      <?php foreach($groups as $g){ ?>	      		
	      		<div class="row panel">
					<div class="small-12 medium-12 large-6 columns"><?php echo $g->id; ?></div>
					<div class="small-12 medium-12 large-6 columns"><?php echo $g->name; ?></div>
					<div class="small-12 medium-12 large-6 columns"><?php echo $g->description; ?></div>
					<div class="small-12 medium-12 large-12 columns">
						<a href="<?php echo site_url('group/update/' . $g->id); ?>" class="button small">Update</a>
						<a href="<?php echo site_url('group/delete/' . $g->id); ?>" class="button small alert delete">Delete</a>
					</div>
				</div>
	      <?php } ?>	      
	</div>
</div>