<div id="seed">
  <h3>
    Please select a list of seeder models to run.
    Filename sequence is important if there are any model dependencies.
  </h3>

  <h4>
    <?php if($success === TRUE){ ?>
      <h3 class="success">Seeding was successful!</h3>
    <?php } else if($success === FALSE){ ?>
      <h3 class="warning">
        Seeding was unsuccessful.<br />
        Please check your models or credentials and try again.
      </h3>
    <?php
      }
      if(isset($truncate_all) && TRUE === $truncate_all){
    ?>
      <h3 class="success">All seeds were initially truncated.</h3>
    <?php } ?>
  </h4>

  <?php 
    echo form_open('seed'),
         $this->load->view('partials/internal_credentials', NULL, TRUE);
  ?>
    
    <input type="checkbox" id="toggle" checked />
    <label for="toggle">Toggle</label>
    
    <ol>
      <?php
        $seeders = directory_map(APPPATH . 'seeders');
        $i_ext = strlen('.php');
        foreach($seeders as $s)
        {
          $readable = substr($s, 0, strlen($s) - $i_ext);
          $readable = str_replace('_', ' ', $readable);
      ?>
          <li>
            <label>
              <input type='checkbox'
                      name='seeders[]'
                      value='<?php echo $s; ?>'
                      checked />
              <?php
                echo humanize($readable);

                if(isset($successful_seeders))
                {
                  if(in_array($s, $successful_seeders))
                  {
                    echo $this->typography->format_characters(
                      '<span class="info">  --ran successfully.</span>'
                    );
                  }
                }
              ?>

            </label>
          </li>
        <?php } ?>
    </ol>

    <footer>
      <button name="seed">Seed</button>
      <input type="checkbox" name="truncate_all" id="truncate-all" />
      <label for="truncate-all">Initially truncate all existing seeds</label>
    </footer>

  <?php echo form_close(); ?>

<script src="<?php echo base_url('static/components/jquery/dist/jquery.js'); ?>"></script>
<script>
  $(window).load(function(){
    $('#toggle').click(function(){
      $('ol input').prop('checked', $('#toggle').prop('checked'));
    });
  });
</script>
</div>