<div id="permission" class="index row">
	<div class="small-12 medium-12 large-12 columns">
			<h4>Permissions</h4>
	  	<a href="<?php echo site_url('permission/create'); ?>" class="button small">New Permission</a>
		
	      <?php foreach($permissions as $p){ ?>	      		
	      		<div class="row panel">
					<div class="small-12 medium-12 large-6 columns"><?php echo $p->id; ?></div>
					<div class="small-12 medium-12 large-6 columns"><?php echo $p->name; ?></div>
					<div class="small-12 medium-12 large-12 columns">
						<a href="<?php echo site_url('permission/update/' . $p->id); ?>" class="button small">Update</a>
						<a href="<?php echo site_url('permission/delete/' . $p->id); ?>" class="button small alert delete">Delete</a>
					</div>
				</div>
	      <?php } ?>	      
	</div>
</div>