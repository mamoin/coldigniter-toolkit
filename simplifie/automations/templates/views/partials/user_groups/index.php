<div id="user_group" class="index row">
	<div class="small-12 medium-12 large-12 columns">
			<h4>User Groups</h4>
	  	<a href="<?php echo site_url('usergroup/create'); ?>" class="button small">New User Group</a>
		
	      <?php foreach($userGroups as $u){ ?>	      		
	      		<div class="row panel">
					<div class="small-12 medium-12 large-6 columns"><?php echo $u->id; ?></div>
					<div class="small-12 medium-12 large-6 columns"><?php echo $u->; ?></div>
					<div class="small-12 medium-12 large-6 columns"><?php echo $u->; ?></div>
					<div class="small-12 medium-12 large-12 columns">
						<a href="<?php echo site_url('usergroup/update/' . $u->id); ?>" class="button small">Update</a>
						<a href="<?php echo site_url('usergroup/delete/' . $u->id); ?>" class="button small alert delete">Delete</a>
					</div>
				</div>
	      <?php } ?>	      
	</div>
</div>