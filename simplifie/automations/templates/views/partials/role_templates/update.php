<div id="roletemplate" class="update row">
  <div class="small-12 medium-12 large-12 columns">
    <h4>Update Roletemplate</h4>
        <?php 
        if(isset($status))
        {
          echo $this->load->view
          (
            'commons/partials/header_messages', 
            array('status' => $status), 
            true
          );
        }
        echo form_open('roletemplate/update'); 
      ?>      
      <div class="row">  
          <div class="small-12 medium-12 large-12 columns">
            Id: <input type="text" name="id" value="<?php echo set_value('id', $roletemplate->id); ?>" />        
        </div>
          <div class="small-12 medium-12 large-12 columns">
            :         
        </div>
          <div class="small-12 medium-12 large-12 columns">
            Description: <textarea name="description"><?php echo set_value('description', $roletemplate->description); ?></textarea>        
        </div>
          <div class="small-12 medium-12 large-12 columns">
            Enabled: <input type="checkbox" name="enabled" checked="<?php echo set_value('enabled',  $roletemplate->enabled); ?>" />        
        </div>
          <div class="small-12 medium-12 large-12 columns">
            :         
        </div>
        </div>
      <div class="row">
        <div class="small-12 medium-12 large-12 columns">
          <a href="<?php echo site_url('roletemplate/read/'  . $roletemplate->id); ?>" class="button small alert">Back</a>
          <button class="button small">Update</button>
        </div>
      </div>
    </form>
  </div>
</div>