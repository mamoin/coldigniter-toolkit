<div id="roletemplate" class="index row">
	<div class="small-12 medium-12 large-12 columns">
			<h4>Roletemplates</h4>
	  	<a href="<?php echo site_url('roletemplate/create'); ?>" class="button small">New Roletemplate</a>
		
	      <?php foreach($roletemplates as $r){ ?>	      		
	      		<div class="row panel">
					<div class="small-12 medium-12 large-6 columns"><?php echo $r->id; ?></div>
					<div class="small-12 medium-12 large-6 columns"><?php echo $r->; ?></div>
					<div class="small-12 medium-12 large-6 columns"><?php echo $r->description; ?></div>
					<div class="small-12 medium-12 large-6 columns"><?php echo $r->enabled; ?></div>
					<div class="small-12 medium-12 large-6 columns"><?php echo $r->; ?></div>
					<div class="small-12 medium-12 large-12 columns">
						<a href="<?php echo site_url('roletemplate/update/' . $r->id); ?>" class="button small">Update</a>
						<a href="<?php echo site_url('roletemplate/delete/' . $r->id); ?>" class="button small alert delete">Delete</a>
					</div>
				</div>
	      <?php } ?>	      
	</div>
</div>