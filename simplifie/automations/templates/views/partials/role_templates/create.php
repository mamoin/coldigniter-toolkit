<div id="roletemplate" class="create row">
  <div class="small-12 medium-12 large-12 columns">
    <h4>New Roletemplate</h4>
        <?php 
        if(isset($status))
        {
          echo $this->load->view
          (
            'commons/partials/header_messages', 
            array('status' => $status), 
            true
          );
        }
        echo form_open('roletemplate/create'); 
      ?>
      <div class="row">
          <div class="small-12 medium-12 large-12 columns">        
            Id: <input type="text" name="id" />        
        </div>  
          <div class="small-12 medium-12 large-12 columns">        
            :         
        </div>  
          <div class="small-12 medium-12 large-12 columns">        
            Description: <textarea name="description"></textarea>        
        </div>  
          <div class="small-12 medium-12 large-12 columns">        
            Enabled: <input type="checkbox" name="enabled" />        
        </div>  
          <div class="small-12 medium-12 large-12 columns">        
            :         
        </div>  
            <div class="small-12 medium-12 large-12 columns">
          <a href="<?php echo site_url('roletemplate'); ?>" class="button small alert">Cancel</a>
          <button class="button small">Create</button>
        </div>
      </div>
    </form>
  </div>
</div>