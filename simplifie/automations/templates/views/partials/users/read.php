<div id="user" class="read row">
  <div class="small-12 medium-12 large-12 columns">
    <h4>User</h4>
        <div class="row">
      <div class="small-12 medium-12 large-12 columns">  
          Id: <?php echo $user->id; ?>
</div>
      <div class="small-12 medium-12 large-12 columns">  
          Full Name: <?php echo $user->full_name; ?>
</div>
      <div class="small-12 medium-12 large-12 columns">  
          Email: <?php echo $user->email; ?>
</div>
      <div class="small-12 medium-12 large-12 columns">  
          Password: <?php echo $user->password; ?>
</div>
      <div class="small-12 medium-12 large-12 columns">  
          Gender: <?php echo $user->gender; ?>
</div>
      <div class="small-12 medium-12 large-12 columns">  
          Enabled: <?php echo $user->enabled; ?>
</div>
    </div>
    <div class="row">
      <div class="small-12 medium-12 large-12 columns">
        <a href="<?php echo site_url('user'); ?>" class="button small alert">Back</a>
        <a href="<?php echo site_url('user/update/' . $user->id); ?>" class="button small">Update</a>
      </div>
    </div>
  </div>
</div>