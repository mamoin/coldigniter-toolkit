<div id="user" class="update row">
  <div class="small-12 medium-12 large-12 columns">
    <h4>Update User</h4>
        <?php 
        if(isset($status))
        {
          echo $this->load->view
          (
            'commons/partials/header_messages', 
            array('status' => $status), 
            true
          );
        }
        echo form_open('user/update'); 
      ?>      
      <div class="row">  
          <div class="small-12 medium-12 large-12 columns">
            Id: <input type="text" name="id" value="<?php echo set_value('id', $user->id); ?>" />        
        </div>
          <div class="small-12 medium-12 large-12 columns">
            Full Name: <input type="text" name="full_name" value="<?php echo set_value('full_name', $user->full_name); ?>" />        
        </div>
          <div class="small-12 medium-12 large-12 columns">
            Email: <input type="text" name="email" value="<?php echo set_value('email', $user->email); ?>" />        
        </div>
          <div class="small-12 medium-12 large-12 columns">
            Password: <input type="text" name="password" value="<?php echo set_value('password', $user->password); ?>" />        
        </div>
          <div class="small-12 medium-12 large-12 columns">
            Gender: <?php echo form_dropdown('gender', $genders, set_value('gender',  $user->gender)); ?>        
        </div>
          <div class="small-12 medium-12 large-12 columns">
            Enabled: <input type="checkbox" name="enabled" checked="<?php echo set_value('enabled',  $user->enabled); ?>" />        
        </div>
        </div>
      <div class="row">
        <div class="small-12 medium-12 large-12 columns">
          <a href="<?php echo site_url('user/read/'  . $user->id); ?>" class="button small alert">Back</a>
          <button class="button small">Update</button>
        </div>
      </div>
    </form>
  </div>
</div>