<div id="user" class="index row">
	<div class="small-12 medium-12 large-12 columns">
			<h4>Users</h4>
	  	<a href="<?php echo site_url('user/create'); ?>" class="button small">New User</a>
		
	      <?php foreach($users as $u){ ?>	      		
	      		<div class="row panel">
					<div class="small-12 medium-12 large-6 columns"><?php echo $u->id; ?></div>
					<div class="small-12 medium-12 large-6 columns"><?php echo $u->full_name; ?></div>
					<div class="small-12 medium-12 large-6 columns"><?php echo $u->email; ?></div>
					<div class="small-12 medium-12 large-6 columns"><?php echo $u->password; ?></div>
					<div class="small-12 medium-12 large-6 columns"><?php echo $u->gender; ?></div>
					<div class="small-12 medium-12 large-6 columns"><?php echo $u->enabled; ?></div>
					<div class="small-12 medium-12 large-12 columns">
						<a href="<?php echo site_url('user/update/' . $u->id); ?>" class="button small">Update</a>
						<a href="<?php echo site_url('user/delete/' . $u->id); ?>" class="button small alert delete">Delete</a>
					</div>
				</div>
	      <?php } ?>	      
	</div>
</div>