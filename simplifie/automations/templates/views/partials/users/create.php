<div id="user" class="create row">
  <div class="small-12 medium-12 large-12 columns">
    <h4>New User</h4>
        <?php 
        if(isset($status))
        {
          echo $this->load->view
          (
            'commons/partials/header_messages', 
            array('status' => $status), 
            true
          );
        }
        echo form_open('user/create'); 
      ?>
      <div class="row">
          <div class="small-12 medium-12 large-12 columns">        
            Id: <input type="text" name="id" />        
        </div>  
          <div class="small-12 medium-12 large-12 columns">        
            Full Name: <input type="text" name="full_name" />        
        </div>  
          <div class="small-12 medium-12 large-12 columns">        
            Email: <input type="text" name="email" />        
        </div>  
          <div class="small-12 medium-12 large-12 columns">        
            Password: <input type="text" name="password" />        
        </div>  
          <div class="small-12 medium-12 large-12 columns">        
            Gender: <?php echo form_dropdown('gender', $genders); ?>        
        </div>  
          <div class="small-12 medium-12 large-12 columns">        
            Enabled: <input type="checkbox" name="enabled" />        
        </div>  
            <div class="small-12 medium-12 large-12 columns">
          <a href="<?php echo site_url('user'); ?>" class="button small alert">Cancel</a>
          <button class="button small">Create</button>
        </div>
      </div>
    </form>
  </div>
</div>