<div id="migrate">
  <div><?php if(isset($message)) echo $message; ?></div>
  <div>
    The database is currently at version <?php echo $current; ?>.
    <br />
    Ideal version is <?php echo $ideal; ?>.
    <a href="<?php echo site_url('migrate/current'); ?>">Set to ideal version</a>.
  </div>

  <?php
    $sec = $this->security;
    $csrf =
    [
      $sec->get_csrf_token_name() =>
      $sec->get_csrf_hash()
    ];
    echo form_open('migrate', $current, $csrf),
         $this->load->view('partials/internal_credentials', NULL, TRUE);
  ?>
    <div>
      <label>Migrate to version:</label>
      <?php
        echo form_dropdown('version', $migrations, $current);
      ?>
      <input type="submit" value="Go" />
    </div>
  <?php echo form_close(); ?>
</div>