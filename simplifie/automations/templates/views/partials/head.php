<head>
  <meta charset="utf-8" />
  <title><?php echo $title; ?></title>
  <meta content="" name="description" />
  <meta content="" name="author" />
  <meta property="og:url" content="" />
  <meta property="og:type" content="" />
  <meta property="og:title" content="" />
  <meta property="og:description" content="" />
  <meta property="og:image" content="" />
  <?php echo $css; ?>
</head>