<div id="privilege" class="update row">
  <div class="small-12 medium-12 large-12 columns">
    <h4>Update Privilege</h4>
        <?php 
        if(isset($status))
        {
          echo $this->load->view
          (
            'commons/partials/header_messages', 
            array('status' => $status), 
            true
          );
        }
        echo form_open('privilege/update'); 
      ?>      
      <div class="row">  
          <div class="small-12 medium-12 large-12 columns">
            Id: <input type="text" name="id" value="<?php echo set_value('id', $privilege->id); ?>" />        
        </div>
          <div class="small-12 medium-12 large-12 columns">
            Name: <input type="text" name="name" value="<?php echo set_value('name', $privilege->name); ?>" />        
        </div>
        </div>
      <div class="row">
        <div class="small-12 medium-12 large-12 columns">
          <a href="<?php echo site_url('privilege/read/'  . $privilege->id); ?>" class="button small alert">Back</a>
          <button class="button small">Update</button>
        </div>
      </div>
    </form>
  </div>
</div>