<div id="privilege" class="index row">
	<div class="small-12 medium-12 large-12 columns">
			<h4>Privileges</h4>
	  	<a href="<?php echo site_url('privilege/create'); ?>" class="button small">New Privilege</a>
		
	      <?php foreach($privileges as $p){ ?>	      		
	      		<div class="row panel">
					<div class="small-12 medium-12 large-6 columns"><?php echo $p->id; ?></div>
					<div class="small-12 medium-12 large-6 columns"><?php echo $p->name; ?></div>
					<div class="small-12 medium-12 large-12 columns">
						<a href="<?php echo site_url('privilege/update/' . $p->id); ?>" class="button small">Update</a>
						<a href="<?php echo site_url('privilege/delete/' . $p->id); ?>" class="button small alert delete">Delete</a>
					</div>
				</div>
	      <?php } ?>	      
	</div>
</div>