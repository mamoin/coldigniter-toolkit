<div id="rolepermissiontemplate" class="update row">
  <div class="small-12 medium-12 large-12 columns">
    <h4>Update Rolepermissiontemplate</h4>
        <?php 
        if(isset($status))
        {
          echo $this->load->view
          (
            'commons/partials/header_messages', 
            array('status' => $status), 
            true
          );
        }
        echo form_open('rolepermissiontemplate/update'); 
      ?>      
      <div class="row">  
          <div class="small-12 medium-12 large-12 columns">
            Id: <input type="text" name="id" value="<?php echo set_value('id', $rolepermissiontemplate->id); ?>" />        
        </div>
          <div class="small-12 medium-12 large-12 columns">
            :         
        </div>
          <div class="small-12 medium-12 large-12 columns">
            Description: <textarea name="description"><?php echo set_value('description', $rolepermissiontemplate->description); ?></textarea>        
        </div>
          <div class="small-12 medium-12 large-12 columns">
            Enabled: <input type="checkbox" name="enabled" checked="<?php echo set_value('enabled',  $rolepermissiontemplate->enabled); ?>" />        
        </div>
          <div class="small-12 medium-12 large-12 columns">
            :         
        </div>
        </div>
      <div class="row">
        <div class="small-12 medium-12 large-12 columns">
          <a href="<?php echo site_url('rolepermissiontemplate/read/'  . $rolepermissiontemplate->id); ?>" class="button small alert">Back</a>
          <button class="button small">Update</button>
        </div>
      </div>
    </form>
  </div>
</div>