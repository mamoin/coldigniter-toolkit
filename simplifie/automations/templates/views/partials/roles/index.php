<div id="role" class="index row">
	<div class="small-12 medium-12 large-12 columns">
			<h4>Roles</h4>
	  	<a href="<?php echo site_url('role/create'); ?>" class="button small">New Role</a>
		
	      <?php foreach($roles as $r){ ?>	      		
	      		<div class="row panel">
					<div class="small-12 medium-12 large-6 columns"><?php echo $r->id; ?></div>
					<div class="small-12 medium-12 large-6 columns"><?php echo $r->name; ?></div>
					<div class="small-12 medium-12 large-12 columns">
						<a href="<?php echo site_url('role/update/' . $r->id); ?>" class="button small">Update</a>
						<a href="<?php echo site_url('role/delete/' . $r->id); ?>" class="button small alert delete">Delete</a>
					</div>
				</div>
	      <?php } ?>	      
	</div>
</div>