<div id="role" class="create row">
  <div class="small-12 medium-12 large-12 columns">
    <h4>New Role</h4>
        <?php 
        if(isset($status))
        {
          echo $this->load->view
          (
            'commons/partials/header_messages', 
            array('status' => $status), 
            true
          );
        }
        echo form_open('role/create'); 
      ?>
      <div class="row">
          <div class="small-12 medium-12 large-12 columns">        
            Id: <input type="text" name="id" />        
        </div>  
          <div class="small-12 medium-12 large-12 columns">        
            Name: <input type="text" name="name" />        
        </div>  
            <div class="small-12 medium-12 large-12 columns">
          <a href="<?php echo site_url('role'); ?>" class="button small alert">Cancel</a>
          <button class="button small">Create</button>
        </div>
      </div>
    </form>
  </div>
</div>