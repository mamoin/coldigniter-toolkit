<!DOCTYPE html>
<html lang="en">
  <head>
    <title><?php echo $title; ?></title>
    <style type="text/css">
      body
      {
        background-color: #fff;
        margin: 40px;
        font: 13px/20px normal Helvetica, Arial, sans-serif;
        color: #4F5155;
      }
      a
      {
        color: #ff6600;
        background-color: transparent;
        font-weight: normal;
        text-decoration: none;
      }
      a:hover{text-decoration: underline;}
      h1
      {
        color: #444;
        background-color: transparent;
        font-size: 19px;
        font-weight: normal;
        margin: 0;
        padding: 14px 15px 4px 15px;
        padding-left: 0;
      }
      .header
      {
        border-bottom: 1px solid #D0D0D0;
        margin-bottom: 4px;
      }
      .header nav{margin: 4px 0;}
      h1, h3, h4{font-weight: normal;}
      h3{line-height: 16px;}
      code
      {
        font-family: Consolas, Monaco, Courier New, Courier, monospace;
        font-size: 12px;
        background-color: #f9f9f9;
        border: 1px solid #D0D0D0;
        color: #002166;
        display: block;
        margin: 14px 0 14px 0;
        padding: 12px 10px 12px 10px;
      }
      #internal
      {
        margin: 10px;
        border: 1px solid #D0D0D0;
        -webkit-box-shadow: 0 0 8px #D0D0D0;
        padding-left: 12px;
        padding-right: 12px;
      }
      span{color: #B0B0B0;}
      p {margin: 12px 15px 12px 15px;}
      input {font-size: 13px;}
      h3, h4{margin: 8px 0;}
      ol
      {
        padding-left: 28px;
        margin-top: 0;
      }
      input[type="checkbox"]
      {
        position: relative;
        top: 2px;
      }
      #toggle{margin-left: 32px;}
      input[type="submit"],
      select{margin: 8px 0;}
      .success{color: #3c763d;}
      .warning{color: #a94442;}
      .info{color: #31708f;}
      footer{margin: 12px 0;}
    </style>
  </head>
  <body>
    <div id="internal">
      <div class="header">
        <h1>
          <a href="<?php echo site_url(); ?>">Home</a> |
          Database <?php echo $title; ?>
        </h1>
        <nav>
          <a href="<?php echo site_url('migrate'); ?>">Migrate</a> |
          <a href="<?php echo site_url('seed'); ?>">Seed</a>
        </nav>
      </div>