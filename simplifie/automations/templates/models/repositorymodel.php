<?php  
  if(!defined('BASEPATH')) exit('Direct script access not allowed.');
  /**
   * Use this class as a base model to get
   * built-in CRUD and other data mechanisms.
   */
  class RepositoryModel extends CI_Model
  {
    private $sTblName;
    public function __construct()
    {
      parent::__construct();
    }
    public final function setTableName($tableName)
    {
      $this->sTblName = $tableName;
    }
    public final function getTableName()
    {
      return $this->sTblName;
    }
    public final function index()
    {
      return $this->db->get($this->getTableName());
    }
    public final function create($data)
    {
      $this->db->insert
      (
        $this->getTableName(), 
        $data
      );
      return $this->read($this->db->insert_id());
    }
    public final function read($id)
    {
      return $this->db->get_where
      (
        $this->getTableName(), 
        ['id' => $id]
      );
    }
    public final function readBy($field, $value)
    {
      return $this->db
        ->where($field, $value)
        ->get($this->getTableName());
    }
    public final function update($id, $data)
    {
      $this->db->where('id', $id);
      $this->db->update
      (
        $this->getTableName(), 
        $data
      );
      return $this->db->affected_rows();
    }
    public final function delete($id)
    {
      return $this->db
        ->where('id', $id)
        ->delete($this->getTableName());
    }
  }