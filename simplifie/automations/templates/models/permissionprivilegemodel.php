<?php
  if(!defined('BASEPATH')) exit('Direct script access not allowed');
  class PermissionPrivilegeModel extends CI_Model
  {
    public function __construct()
    {
      parent::__construct();
    }
    public final function index()
    {
      $this->db->select('p.*');
      $this->db->from('permissionprivileges p');
      return $this->db->get();
    }
    public final function create()
    {
      $i = $this->input;
      $this->db->insert
      (
        'permissionprivileges', 
        getPostValuePair()
      );
      return $this->read($this->db->insert_id());
    }
    public final function read($id)
    {
        return $this->db->get_where
        (
          'permissionprivileges', 
          array('id' => $id)
        );
    }
    
    public final function update()
    {
      $i = $this->input;
      $id = $i->post('id');
      $this->db->where('id', $id);
      $this->db->update
      (
        'permissionprivileges', 
        getPostValuePair()
      );
    }
    public final function delete($id)
    {
      $this->db->where('id', $id);
      return $this->db->delete('permissionprivileges');
    }
    public final function readPermission($userId, $permission, $privileges)
    {
      $r = $this->db
        ->select('*')
        ->from('permissionprivileges pp')
        ->join('permissions pe', 'pp.permission_id = pe.id')
        ->join('privileges pr', 'pp.privilege_id = pr.id')
        ->where('pp.user_id', $userId)
        ->where('pe.name', $permission)
        ->where_in('pe.name', $privileges)
        ->get()
        ->result();
      return $r;
    }
  }