<?php
  if(!defined('BASEPATH')) exit('No direct script access allowed');
  class SeederModel extends CI_Model
  {
    private $sTblName;
    public function __construct()
    {
      parent::__construct();
    }
    public final function truncate()
    {
      if(empty($this->getTableName()))
      {
        throw new ErrorException('Table name not set.');
      }
      else
      {
        $this->db->truncate($this->getTableName());
      }
    }
    public function run($direction)
    {
      throw new ErrorException('Method unimplemented.');
    }
    protected final function setTableName($name)
    {
      $this->sTblName = $name;
    }
    protected final function getTableName()
    {
      return $this->sTblName;
    }
    protected final function get_id
    (
      $tableName,
      $idField,
      $fieldName,
      $fieldValue
    )
    {
      $this->db->select($idField);
      $this->db->from($tableName);
      $this->db->where($fieldName, $fieldValue);
      return $this->db->get()->row()->{$idField};
    }
  }