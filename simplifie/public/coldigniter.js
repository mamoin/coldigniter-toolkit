function ColdIgniter()
{
  this.init = function()
  {
    this.setListeners();
  };
  this.setListeners = function()
  {
    $('#seeders .toggle').click
    (
      function()
      {
        var t = $(this);
        $('#seeders .seeders').each
        (
          function()
          {
            //
          }
        );
      }
    );
    //
    $('#permissions select.roles').click
    (
      function()
      {
        var t = $(this);
        var roleName = t.val();
        var uId = $('#userId').val();
        var s = 'user/permission/updateBy/roleName/userId/' 
          + roleName + '/' + uId;
        $.ajax
        (
          {
            url: s, 
            success: function(response)
            {
              //Reset all permission privileges 
              //based on selected role.
            }
          }
        );
      }
    );
    $('#permissions .permission .privilege').click
    (
      function()
      {
        var t = $(this);
        var permId = t.parent().data('id');
        var privId = t.data('id');
        var s = '/permission/privilege/update' + 
          permId + '/' + privId;
        $.ajax({url: s});
      }
    );
  };
}
//
new ColdIgniter().init();