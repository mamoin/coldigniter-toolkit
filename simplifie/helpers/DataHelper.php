<?php
  /**
   * DataHelper.
   * @author Mark N. Amoin
  */
  namespace simplifie\helpers;
  class DataHelper
  {
    public function __construct(){}
    /**
     * Retrives data from POST method with hash indices.
     * This is useful when the HTML form contains a lot of
     * fields and you only need to retrieve a few of them.
     * 
     * Example:
     * A student form was submitted and contains 
     * full_name, email, password, age, course.
     * 
     * <code>
     * $u = getPostValuePair(array('course'));
     * $this->db->insert('users', $a);
     * 
     * $c = $this->input->post('course');
     * $i = $this->db->insert_id();
     * $this-db->insert('students', ['course' => $c, 'user_id' => $i]);
     * </code>
     * 
     * @param array $excludeList Removes the data
     * of specific hash names.
     * @return array All POST data except the ones
     * listed in the $excludeList parameter.
     */
    public static function getPostValuePair($excludeList = array())
    {
      $CI = get_instance();
      $post = $CI->input->post();
      $list = array();
      foreach($post as $key => $value)
      {
        if(!in_array($key, $excludeList))
        {
          $list[$key] = $value;
        }
      }
      return $list;
    }

    /**
     * Unsupported method of CI 2+.
     * Retrieves data other than GET and POST requests.
     * Normally used for REST DELETE method.
     */
    public static function getInputStream()
    {
      //Stream can only be read once. Use session.
      $CI = get_instance();
      $CI->load->library('session');
      $s = file_get_contents('php://input');
      if(strlen($s) > 0)
      {
          $CI->session->unset_userdata('singleton_stream');
          parse_str($s, $a);
          $CI->session->set_userdata(array('singleton_stream' => $a));
      }
      return $CI->session->userdata('singleton_stream');
    }

    /**
    * Serializes string-based URL params into array. The parameter 
    * must be at the last portion of the parameter list.
    * @param    $fromIndex    1-based index.
    * @return   array.
    */
    public static function getArrayParams($fromIndex)
    {
      $CI = get_instance();
      $a = $CI->uri->segment_array();
      $toIndex = count($a) - 1;
      $a = array_slice($a, $fromIndex, $toIndex);
      return $a;
    }
    /**
     * Single file upload.
     * @param String $fileName The name of form view's input type file.
     * @return array Details of the uploaded file.
     */
    public static function upload($fieldName)
    {
      if($_FILES[$fieldName]['error'] < 1)
      {
        $config['upload_path'] = APPPATH . 'uploads/';
        $config['allowed_types'] = 'gif|jpg|png|pdf|doc';
        $config['encrypt_name'] = true;
        //
        $CI = get_instance();
        $CI->load->library('upload', $config);
        if($CI->upload->do_upload($fieldName))
        {
          return $CI->upload->data();
        }
        else
        {
          return $CI->upload->display_errors();
        }
      }
      else
      {
        return null;
      }
    }
    /**
     * Multiple file upload.
     * @param String $fileName The name of form view's input type file.
     * @return array Details of the uploaded file.
     */
    public static function multiUpload($fieldName = 'files')
    {
      $CI = get_instance();
      $CI->load->library('multi_upload/my_upload');
      $c = array
      (
        'upload_path' => APPPATH . 'uploads/', 
        //'allowed_types' => 'gif|jpg|png',
        'encrypt_name' => true
      );
      $CI->my_upload->initialize($c);
      if($CI->my_upload->do_multi_upload($fieldName))
      {
        $data = $CI->my_upload->get_multi_upload_data();
        $l = array();
        foreach($data as $d)
        {
          $a = array
          (
            'filename' => $d['file_name'], 
            'original_filename' => $d['orig_name']
          );
          array_push($l, $a);
        }
        return $l;
      }
      else
      {
        show_error($CI->my_upload->display_errors()); 
      } 
    }
    /**
     * Downloads a file using a given updated filename.
     * @param String $updatedFilename The new file name that was given
     * to the file when it was uploaded.
     * @return NULL
     */
    public static function downloadFile($updatedFilename)
    {
       get_instance()->load->helper('download');
       $r = $this->read_details_by_updatedFilename($updatedFilename);
       $file = $this->getUploadPath() . $r->updated_filename;
       $data = file_get_contents($file);
       if(!empty($data))
       {
           force_download($r->original_filename, $data);
       }
       else
       {
           throw new ErrorException('Error 06bffcf');
       }
    }
    /**
      * Converts to hash-original-filename.extension convention.
      * @param String $originalFilename The original name of the uploaded file.
      * @return String Returns a new, uniquely-tokenized file name.
    */
    public static function generateUniqueFilename($originalFilename)
    {
       $ext = pathinfo($originalFilename, PATHINFO_EXTENSION);
       $s = $this->slugify($originalFilename);
       $t = $s . date('h:i:sa') . rand(0, 99999);
       return hash('sha256', $t) . '-' . $s . '.' . $ext;
    }
    public static function getUploadPath()
    {
        return APPPATH . 'uploads/';
    }
    /**
      * Converts non-safe URLs to dash symbols.
      * @param String $text The string to convert.
      * @return String Returns a URL-safe converted string.
      * @see http://stackoverflow.com/questions/3984983/php-code-to-generate-safe-url
    */
    public static function slugify($text)
    {
        //Swap out Non "Letters" with a -
        $text = preg_replace('/[^\\pL\d]+/u', '-', $text);
        $text = trim($text, '-');
        //Convert letters that we have left to 
        //the closest ASCII representation
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
        $text = strtolower($text);
        //Strip out anything we haven't been able to convert
        $text = preg_replace('/[^-\w]+/', '', $text);
        return $text;
    }
    /**
     * Simple emailer function.
     * TODO: Replace with another library that 
     * works when using Google email accounts.
     */
    public static function sendEmailer
    (
      $subject, 
      $from, 
      $to, 
      $message, 
      $cc = null,
      $bcc = null
    )
    {
      $CI = get_instance();
      $CI->email->set_newline("\r\n");
      $CI->email->from($from);
      $CI->email->to($to);
      $CI->email->bcc($cc);
      $CI->email->subject($subject);
      $CI->email->message($message);
      $CI->email->send();
      if(!$CI->email->send())
      {
        show_error($CI->email->print_debugger());
        exit;
      }
    }
    /**
     * Generates a sha256 random token for any use.
     * @param String $key Any simple or complicated
     * random string that can be added to generate a new key.
     * @return String A new generated key.
     */
    public static function generateToken($key)
    {
      $s = $key . date('Ymd') . rand(rand(0, 999), 999) . time();
      return hash('sha256', $s);
    }
    /**
     * Determines if the current environment is white-listed.
     * Otherwise shows an error message and exits the
     * entire application.
     * @return NULL
    */
    public static function checkIsWhitelisted()
    {
      $CI = get_instance();
      $CI->load->config('coldigniter');
      $l = $CI->config->item('whitelistEnvironments');
      if(!in_array(ENVIRONMENT, $l))
      {
        exit('Operations can only be performed by Administrators.');
      }
    }
  }