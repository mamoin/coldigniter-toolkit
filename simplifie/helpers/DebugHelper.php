<?php
  /**
   * DebugHelper.
   * @author Mark N. Amoin
  */
  namespace simplifie\helpers;
  class DebugHelper
  {
    public function __construct(){}
    public static function show($data, $forceExit = TRUE)
    {
      echo '<pre>';
      print_r($data);
      echo '</pre>';
      if(TRUE === $forceExit) exit;
    }
    public static function stackTrace()
    {
      DebugHelper::show(debug_backtrace());
    }
    public static function lastQuery()
    {
      DebugHelper::show(get_instance()->db->last_query());
    }
    public static function console($data, $message = '')
    {
      $s = is_array($data) || is_object($data) ? 
        json_encode($data) : 
        $data;
      echo("<script>console.log('PHP $message: ', " . $s . ");</script>");
    }
  }