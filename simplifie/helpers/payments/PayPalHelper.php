<?php
  /**
   * PayPalHelper.
   * Contains methods necessary for fast development.
   * @author Mark N. Amoin
  */
  namespace simplifie\helpers\payments;
  use simplifie\helpers\DataHelper;
  class PayPalHelper
  {
    public function __construct()
    {
      //Test.
      //$this->verifyIpn();
      //
      if($this->validate())
      {
        $this->verifyIpn();
      }
      else
      {
        exit('Invalid PayPal.');
      }
    }
    public final function processPayment()
    {
      //Processing payment.
      $token = generateToken(uniqid(mt_rand(), TRUE));
      $CI = get_instance();
      $i = $CI->input;
      //
      $transMdl = new TransactionModel();
      $transMdl->create($token);
      $count = mysql_affected_rows();
      //
      if($count > 0)
      {
        //Check if product is downloadable or not to select appropriate email.
        $prod = new ProductModel();
        $prodResult = $prod->readByCode($i->post('item_number'));
        $prodIsDownloadable = mysql_fetch_object($prodResult)->downloadable;
        
        //Product is downloadable or available.
        if($prodIsDownloadable > 0)
        {
          $a = 
          [
            'token' => $token, 
            'maxDownloadCount' => 3,
            'daysAvailable' => 3
          ];
        }
        else
        {
          $a = ['productName' => $i->post('item_name')];
        }
        //Transaction success. Sending email and token to paying customer.
        DataHelper::sendEmailer
        (
          'PayPal Transaction',
          'from',
          $i->request('payer_email'),
          $CI->parser->parse('', $a)
        );
      }
      else
      {
        exit('Error creating transaction.');
      }
    }
    //@meth   confirm   Post back to 
    //PayPal system to validate.
    private final function confirm($queryString)
    {
      $header = "POST /cgi-bin/webscr HTTP/1.0\r\n";
      $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
      $header .= 'Content-Length: ' . strlen($queryString) . "\r\n\r\n";
      $fp = fsockopen('www.paypal.com', 80, $errno, $errstr, 30);
      //or fatal_error(__FILE__, __LINE__, "Can't open socket. $errstr (err no:$errno)");
      fputs($fp, $header . $queryString);
      //or fatal_error(__FILE__, __LINE__, "Error while writing to socket.");
      while(!feof($fp))
      {
        $res = fgets ($fp, 1024);
        if(strcmp($res, 'VERIFIED') == 0) 
        {
          return TRUE;
        }
        else if(strcmp($res, 'INVALID') == 0)
        {
          exit('PayPal returned INVALID');
        }
      }
      fclose($fp);
    }
    private final function validate()
    {
      //Validating PayPal.
      //Read the post from PayPal system and add 'cmd'.
      $cmd = 'cmd=_notify-validate';
      $queryString = $cmd;
      foreach($_REQUEST as $key => $value) 
      {
        $value = urlencode(stripslashes($value));
        $queryString .= "&$key=$value";
      }
      $data = str_replace($cmd . '&', '', $queryString);
      //PayPal cmd data:  $data;
      $curl = curl_init();
      curl_setopt($curl, CURLOPT_URL, 'https://www.paypal.com/cgi-bin/webscr');
      //curl_setopt($curl, CURLOPT_URL, 'https://www.sandbox.paypal.com/cgi-bin/webscr');
      curl_setopt($curl, CURLOPT_HEADER, 0);
      curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($curl, CURLOPT_POST, 1);
      curl_setopt($curl, CURLOPT_POSTFIELDS, $queryString);
      //
      if(!($result = curl_exec($curl)))
      {
        //curl_exec error: curl_error($curl);
        return FALSE;
      }
      curl_close($curl);
      //
      if(eregi('VERIFIED', $result))
      {
        //PayPal verified TRUE.
        return TRUE;
      }
      else 
      {
        //PayPal returned INVALID. Data: $data. cURL result: $result
        return FALSE;
      }
    }
    /*
      Useful PayPal IPN params:
      $_POST['payer_email']       = 'paypal@email.account.com'
      $_POST['option_selection1'] = 'email@email.com'
      $_POST['option_selection2'] = 'ppc name';
      $_POST['first_name']        = 'first';
      $_POST['last_name']         = 'last';
      $_POST['item_number']       = 'TG123';
      $_POST['payment_status']    = 'Completed';
      $_POST['mc_gross']          = '14.97';
      $_POST['mc_currency']       = 'SGD';
    */
    private final function verifyIpn()
    {
      $CI = get_instance();
      $i = $CI->input;
      $prodMdl = new ProductModel();
      $result = $prodMdl->getByCode($i->post('item_number'));
      $prod = mysql_fetch_object($result);
      $errMsg = null;
      if($prod->id < 1)
      {
        $errMsg = 'Product does not exist: ' . $i->post('item_number');
      }
      else if
      (
        $i->post('payment_status') != 'Completed' || 
        $i->post('payment_status') != 'Refunded'
      )
      {
        $errMsg = 'Status not completed';
      }
      else
      {
        //No errors. Continue with payment.
        $this->processPayment();
      }
      //
      if($errMsg)
      {
        //Something is wrong. Notify admin.
        $s  = "$errMsg <br /><br />";
        $s .= "Product: {$i->post('item_number')}<br />";
        $s .= "Currency: {$i->post('mc_currency')}<br />";
        $s .= "Price : {$i->post('mc_gross')}<br />";
        $s .= "Status: $i->post('payment_status')}<br />";

        DataHelper::sendEmailer
        (
          'Invalid PayPal Transaction',
          'from',
          'to',
          $s
        );
      }
    }
  }