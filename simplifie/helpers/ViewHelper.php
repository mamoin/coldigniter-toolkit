<?php
  /**
   * ViewHelper.
   * @author Mark N. Amoin
  */
  namespace simplifie\helpers;
  use simplifie\helpers\DataHelper;
  class ViewHelper
  {
    public function __construct(){}
    /**
     * [js description]
     * @param  array   $list     [description]
     * @param  boolean $tokenize [description]
     * @return NULL              [description]
     */
    public static function js($list, $tokenize = FALSE)
    {
      $s = '';
      foreach ($list as $l)
      {
        if($tokenize) $l .= '?' . DataHelper::generateToken($l);
        $s .= '<script src="' . $l . '"></script>';
      }
      echo $s;
    }
    /**
     * [css description]
     * @param  array   $list     [description]
     * @param  boolean $tokenize [description]
     * @return NULL              [description]
     */
    public static function css($list, $tokenize = FALSE)
    {
      $s = '';
      foreach ($list as $l)
      {
        if($tokenize) $l .= '?' . DataHelper::generateToken($l);
        $s .= '<link rel="stylesheet" href="' . $l . '" />';
      }
      echo $s;
    }
    public static function show($view, $data = NULL)
    {
      $l = get_instance()->load;
      $l->helper('url');
      $l->view('layouts/header', $data);
      $l->view('partials/' . $view, $data);
      $l->view('layouts/footer', $data);
    }
    public static function showInternal($view, $data = NULL)
    {
      $l = get_instance()->load;
      $l->helper('url');
      $l->view('layouts/internals/header', $data);
      $l->view('partials/' . $view, $data);
      $l->view('layouts/internals/footer', $data);
    }
    public static function json($data, $status = 200)
    {
      exit(get_instance()->output
      ->set_status_header($status)
      ->set_content_type('application/json')
      ->set_output(json_encode($data))
      ->_display());
    }
    /**
     * Provides HTML version of CSRF token containing
     * ID attribute is useful for AJAX requests.
     * 
     * var o = {};
     * o[$('#csrf').attr('name')] = $('#csrf').val();
     * $.ajax({data: o});
     * 
     * @return string
     */
    public static function csrf()
    {
      $CI = get_instance();
      $s = '<input type="hidden" id="csrf" name="' . 
        $CI->security->get_csrf_token_name() . '" value="' . 
        $CI->security->get_csrf_hash() . '" />';
      return $s;
    }
  }