<?php
  /**
   * AuthHelper.
   * Contains methods necessary for fast development.
   * @author Mark N. Amoin
  */
  namespace simplifie\helpers;
  class AuthHelper
  {
    public function __construct(){}
    public static function setSwitchedUser($proxyUserId = NULL)
    {
      $s = get_instance()->session;
      if(empty($id))
      {
        //Logout tmp user as.
        $o = $s->user_data('originalUserId');
        if(!empty($o))
        {
          $s->set_userdata('userId', $o);
          $s->unset_userdata('originalUserId');
        }
      }
      else
      {
        $s->set_userdata('originalUserId', $s->user_data('userId'));
        //Create tmp log user as.
        $s->set_userdata('userId', $id);
      }
    }
    public static function getSwitchedUsers()
    {
      $o = get_instance()->session->user_data('originalUserId');
      if(empty($o))
      {
        return [];
      }
      else
      {
        $a = 
        [
          'original' => $o,
          'proxy' => AuthHelper::getLoggedUser()
        ];
        return $a;
      }
    }
    public static function getLoggedUser()
    {
      return get_instance()->session->userdata('userId');
    }
    public static function login($id)
    {
      $s = get_instance()->session;
      if(!empty($id))
      {
        $s->set_userdata('userId', $id);
      }
      else
      {
        //Logout.
        $s->sess_destroy();
      }
    }
    public static function isLoggedIn()
    {
      return !empty(AuthHelper::getLoggedUser());
    }
    /**
     * Determines of such user has privileges to such permission.
     * 
     * Returns FALSE if Subscriber.
     * AuthHelpler::isPermitted(1, 'User');
     * 
     * Returns TRUE if Super Administrator.
     * AuthHelpler::isPermitted(1, 'User');
     * 
     * Returns TRUE if Administrator.
     * AuthHelpler::isPermitted(1, 'User', ['READ']);
     * 
     * 
     * Returns FALSE if Author.
     * AuthHelpler::isPermitted(1, 'User', ['DELETE']);
     * 
     * @param int     $userId     description
     * @param string  $permission description
     * @param array   $privileges description
     * @return bool
     */
    public static function isPermitted
    (
      $userId,
      $permission, 
      $privileges = ['CREATE', 'READ', 'UPDATE', 'DELETE']
    )
    {
      $CI = get_instance();
      $CI->load->model('permissionprivilegemodel');
      $p = $CI
        ->permissionprivilegemodel
        ->readPermission($userId, $permission, $privileges);
      return $p;
    }
    public static function isInternalAdmin($username = '', $password = '')
    {
      $CI = get_instance();
      $CI->load->config('coldigniter');
      $i = $CI->config->item('internalAdministrators');
      $c = $i['credentials'];
      $s = "$username/$password";
      return in_array($s, $c, TRUE);
    }
    /**
     * Used by internal operations such as migration or seeder.
     * @param  [type] $redirectUrl [description]
     * @return [type]              [description]
     */
    public static function filterIsInternalAdmin($redirectUrl)
    {
      $CI = get_instance();
      $i = $CI->input;
      $u = $i->post('username');
      $p = $i->post('password');
      $b = AuthHelper::isInternalAdmin($u, $p);
      if(!$b && \simplifie\helpers\DataHelper::checkIsWhitelisted())
      {
        $CI->session->set_flashdata('credentials', 'invalid');
        redirect(site_url($redirectUrl));
      }
    }
  }