<?php
  /**
   * ColdIgniter Toolkit
   * A script for Composer used as a deployer 
   * for the entire toolkit. It creates the entire 
   * MVC framework along with the files for rare,
   * but needed files for development such as
   * migrations, seeders and CRUD.
   * @package CodeIgniter 3
   * @subpackage vendors
   * @author Mark N. Amoin
   */
  namespace simplifie;
  use Composer\Script\Event;
  class ColdIgniter
  {
    private $sTmpl;
    private $sCiAppl;
    private $sLvl;
    public function __construct()
    {
      $this->sLvl = './';
      $this->sTmpl = './vendor/prezire/coldigniter-toolkit/simplifie/automations/templates/';
      $this->sCiAppl = $this->sLvl . 'application/';
      $this->createStruct();
    }
    private final function createStruct()
    {
      $this->autoCreateFolders();
      $this->copyFiles('libraries');
      $this->copyFiles('controllers');
      $this->copyFiles('config');
      $this->copyFiles('models');
      $this->copyFiles('migrations');
      $this->copyFiles('seeders');
      $this->copyFiles('views');
      $c = 'public/third_party/coldigniter';
      $this->rCopy($this->sTmpl . $c, $c);
      $this->rCopy($this->sTmpl . 'cruds', $this->sCiAppl . '/third_party/coldigniter/automations/templates/views');
    }
    private final function copyFiles($dirName)
    {
      $s = $this->sTmpl . $dirName;
      $d = $this->sCiAppl . $dirName;
      $this->rCopy($s, $d);
    }
    //Recursive copy.
    //http://php.net/manual/en/function.copy.php#91010
    private final function rCopy($source, $destination)
    { 
      $dir = opendir($source); 
      @mkdir($destination); 
      while(false !== ( $file = readdir($dir))) 
      { 
        if($file != '.' && $file != '..' )
        {
          $s = $source . '/' . $file;
          $d = $destination . '/' . $file;
          if(is_dir($s))
          {
            echo "Copy folder $s to folder $d.\n\r\n\r";
            $this->rCopy($s, $d);
          }
          else
          {
            /*if(file_exists($d))
            {
              echo "File $s exists on folder $d.\n\r\n\r";
            }
            else
            {
              Automtically copy. Dangerous for unsaved progress.
            */
              echo "Copy file $s to folder $d.\n\r\n\r";
              copy($s, $d);
            //}
          }
        } 
      } 
      closedir($dir); 
    }
    //Create folders not found in the templates folder.
    private final function autoCreateFolders()
    {
      $list = 
      [
        'uploads',
        'models/seeders',
        'third_party/coldigniter/automations/cruds',
        'third_party/coldigniter/automations/templates/views'
      ];
      foreach($list as $l)
      {
        $this->createFolder($this->sCiAppl . $l);
      }
      $this->createFolder($this->sLvl . 'public/third_party/coldigniter');
    }
    private final function createFolder($filename)
    {
      if(file_exists($filename))
      {
        echo "Folder $filename exists.\r\n";
      }
      else
      {
        mkdir($filename, NULL, TRUE);
        echo "Folder $filename created.\r\n";
      }
    }
    /**
     * Composer script.
     * @param  Event  $event [description]
     * @return NULL
     */
    public static function deploy(Event $event)
    {
      new ColdIgniter();
    }
  }